<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Guests;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class GuestsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('guestname', TextType::class, array(
                'label' => 'Имя',
//                'attr' => array(
//                    'placeholder' => 'Например: windson5690'
//                ),
                'required' => true
            ))
            ->add('phone', TextType::class, array(
                    'label' => 'Номер телефона',
//                'attr' => array(
//                    'placeholder' => 'Например: +79293215614'
//                )
                )
            );

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Guests::class
        ]);
    }

}