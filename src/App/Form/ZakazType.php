<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Adres;

use App\Entity\Zakaz;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class ZakazType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', TextType::class, array(
                'label' => 'Город*',
//                'attr' => array(
//                    'placeholder' => 'Например: user@user.ru'
//                ),
                'required' => true,
            ))
            ->add('street', TextType::class, array(
                'label' => 'Улица*',
//                'attr' => array(
//                    'placeholder' => 'Например: windson5690'
//                ),
                'required' => true
            ))
            ->add('house', TextType::class, array(
                'label' => 'Дом*',
//                'attr' => array(
//                    'placeholder' => 'Введите фамилию ...',
//                ),
                'required' => true
            ))
            ->add('porch', TextType::class, array(
                'label' => 'Подъезд*',
//                'attr' => array(
//                    'placeholder' => 'Введите имя ...',
//                ),
                'required' => true
            ))
            ->add('apartment', TextType::class, array(
                'label' => 'Квартира*',
//                'attr' => array(
//                    'placeholder' => 'Например: +79293215614'
//                )
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Zakaz::class
        ]);
    }

}