<?php


namespace App\Form;


use App\Entity\Permission;
use App\Entity\Role;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                'label' =>'Name Role'
            ])
            ->add('permissions', EntityType::class, array(
                    'class' => Permission::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'form-control',
                        'style' => 'margin:5px 0;'),

                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Role::class
        ]);
    }

}