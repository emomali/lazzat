<?php


namespace App\Security;

use App\Entity\User;
use App\Services\Logger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    const LOG = 'Система';
    const LOG_NOT_FIND_USER = 'Попытка входа в систему. Неизвестное имя пользователя: %s';
    const LOG_WRONG_PASSWORD = 'Попытка входа в систему. Неверно указан пароль';
    const LOG_DELETE_PATIENT = 'Попытка входа в систему. Курс реабилитации пациента закончен: %s';

    const ERROR_AUTH = 'Неверно указан идентификатор или пароль';
    const ERROR_DELETE_PATIENT = 'Курс реабилитации данного пациента закончен';

    /**
     * @var ContainerInterface
     */
    protected $container;

    private $entityManager;
    private $router;
    private $csrfTokenManager;
    private $passwordEncoder;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager, RouterInterface $router,
                                CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request)
    {
        return 'login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array).
     *
     * @param Request $request
     *
     * @return mixed Any non-null value
     *
     * @throws \UnexpectedValueException If null is returned
     */
    public function getCredentials(Request $request)
    {
        $credentials = [
            'username'   => $request->request->get('_username'),
            'password'   => $request->request->get('_password'),
            'csrf_token' => $request->request->get('_csrf_token'),
            'recaptcha'  => $request->request->get('g-recaptcha-response'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @throws AuthenticationException
     *
     * @return UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);

        if (!$user || $credentials['username'] == 'guest') {
            $guest = $this->entityManager->getRepository(User::class)->findOneBy(['username' => 'guest']);
            $this->container->get(Logger::class)
                ->addLog(self::LOG, sprintf(self::LOG_NOT_FIND_USER, $credentials['username']), $guest);

            throw new CustomUserMessageAuthenticationException(self::ERROR_AUTH);
        }

        return $user;
    }

    /**
     * Returns true if the credentials are valid.
     *
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     *
     * @throws AuthenticationException
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if ($this->passwordEncoder->isPasswordValid($user, $credentials['password'])) {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $user->getUsername()]);

            if (in_array('ROLE_PATIENT', $user->getRoles())) {
                $patient = $this->entityManager->getRepository(User::class)->find($user->getId());

                if (!$patient) {
                    $this->container->get(Logger::class)
                        ->addLog(self::LOG, sprintf(self::LOG_DELETE_PATIENT, $user->getUsername()), $user);

                    throw new AuthenticationException(self::ERROR_DELETE_PATIENT);
                }
            }

            return true;
        }

        $this->container->get(Logger::class)->addLog(self::LOG, self::LOG_WRONG_PASSWORD, $user);

        throw new AuthenticationException(self::ERROR_AUTH);
    }

    /**
     * Called when authentication executed and was successful!
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->router->generate('home'));
    }

    /**
     * Override to change what happens after a bad username/password is submitted.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->hasSession()) {
            $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        }

        $url = $this->getLoginUrl();

        return new RedirectResponse($url);
    }

    /**
     * Return the URL to the login page.
     *
     * @return string
     */
    protected function getLoginUrl()
    {
        return $this->router->generate('login');
    }
}