<?php


namespace App\Repository;

use App\Entity\Logger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


class LoggerRepository extends ServiceEntityRepository
{
    public function __construct(\Doctrine\Common\Persistence\ManagerRegistry $registry)
    {
        parent::__construct($registry, Logger::class);
    }

    public function searchUsernameOrEmail($search, $offset = null, $limit = null) {
        $qb = $this->createQueryBuilder('log')
            ->orderBy('log.id', 'DESC');

        if (false === is_null($offset))
            $qb->setFirstResult($offset);

        if (false === is_null($limit))
            $qb->setMaxResults($limit);

        return $qb
            ->getQuery()
            ->getResult();
    }
}