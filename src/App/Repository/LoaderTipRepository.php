<?php


namespace App\Repository;

use App\Entity\Tovar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method getDoctrine()
 */
class LoaderTipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tovar::class);
    }

    /**
     * @param $id_tip
     * @return Tovar[]
     */
    public function findAllInThisTip($id_tip)
    {
        $qb = $this->createQueryBuilder('t')
            ->join('t.tip', 'tt', Join::WITH)
            ->where('tt.id = :id_tip')
            ->setParameter('id_tip', $id_tip);
//        $query = $qb->getQuery();

        return $qb
            ->getQuery()
            ->getResult();
    }
    /**
     * @param $tip
     * @return Tovar[]
     */
    public function searchTip($tip)
    {
        $qb = $this->createQueryBuilder('t')
            ->join('t.tip', 'tt', Join::WITH)
            ->where('upper(t.name) LIKE upper(:tip)')
            ->orWhere('upper(tt.name) LIKE upper(:tip)')
            ->setParameter('tip','%'.$tip.'%');
        return $qb
            ->getQuery()
            ->getResult();
    }
    /**
     * @param $tip
     * @return Tovar[]
     */
    public function findres($tip)
    {
        $qb = $this->createQueryBuilder('t')
            ->join('t.restoran', 'tt', Join::WITH)
            ->where('tt.id = :tip')
            ->setParameter('tip',$tip);
        return $qb
            ->getQuery()
            ->getResult();
    }
    /**
     * @param $tip
     * @return Tovar[]
     */
    public function livesearch($tip)
    {
        $qb = $this->createQueryBuilder('t')
            ->join('t.tip', 'tt', Join::WITH)
            ->where('upper(t.name) LIKE upper(:tip)')
            ->orWhere('upper(tt.name) LIKE upper(:tip)')
            ->setParameter('tip','%'.$tip.'%');
//        dump($qb->getQuery()->getSQL());
        return $qb
            ->getQuery()
            ->getResult();
    }
    /**
     * @param $tip
     * @return Tovar[]
     */
    public function searchformodalka($tip)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id = :tip')
            ->setParameter('tip',$tip);
        return $qb
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $tovar
     * @param $restoran
     * @return Tovar[]
     */
    public function findifrestoran($tovar,$restoran)
    {
        $qb = $this->createQueryBuilder('t')
            ->join('t.restoran', 'tt', Join::WITH)
            ->Where('t.id = :tovar')
            ->andWhere('tt.id = :restoran')
            ->setParameters(['tovar'=>$tovar,'restoran'=>$restoran]);

        return $qb
            ->getQuery()
            ->getResult();
    }


}