<?php


namespace App\Repository;


use App\Entity\Restoran;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

class LoaderResRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Restoran::class);
    }
    /**
     * @return Restoran[]
     */
    public function findAllRes($id_tip): array
    {
        $qb = $this->createQueryBuilder('r')
            ->leftJoin('r.tovars', 'rr')
            ->leftJoin('rr.tip', 'rrr')
            ->where('rrr.id  = :id_tip')
            ->setParameter('id_tip', $id_tip);
//        $query = $qb->getQuery();

        return $qb
            ->getQuery()
            ->getResult();
    }
//    /**
//     * @return Restoran[]
//     */
//    public function findAllRes($id_tip): array
//    {
//        $qb = $this->createQueryBuilder('r')
//            ->join('r.tovars', 'rr', Join::WITH)
//            ->where('rr.id = :tovar')
//            ->setParameter('tovar', $id_tip);
////        $query = $qb->getQuery();
//
//        return $qb
//            ->getQuery()
//            ->getResult();
//    }

}