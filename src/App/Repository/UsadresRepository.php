<?php


namespace App\Repository;

use App\Entity\UserAdres;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method getDoctrine()
 */
class UsadresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAdres::class);
    }

    /**
     * @param $id_user
     * @return UserAdres[]
     */
    public function findUsadresTip($id_user)
    {
        $qb = $this->createQueryBuilder('us')
//            ->join('us.id_adres', 'tt', Join::WITH)
            ->where('us.id_user = :id_us')
            ->setParameter('id_us', $id_user);
//        $query = $qb->getQuery();

        return $qb
            ->getQuery()
            ->getResult();
    }
}