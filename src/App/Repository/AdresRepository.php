<?php


namespace App\Repository;

use App\Entity\Adres;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method getDoctrine()
 */
class AdresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Adres::class);
    }

    /**
     * @param $id_adres
     * @return Adres[]
     */
    public function findUsadressTip($id_adres)
    {
        $qb = $this->createQueryBuilder('a')
//            ->join('a.', 'tt', Join::WITH)
            ->where('a.id = :id_us')
            ->setParameter('a', $id_adres);
//        $query = $qb->getQuery();

        return $qb
            ->getQuery()
            ->getResult();
    }
}