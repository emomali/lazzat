<?php


namespace App\Services;

use App\Entity\ActionForLog;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;


class Logger
{
    private $entityManager;

    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }


    public function addLog($action, $info){
        if($action==""){
            return;
        }
        $session = new Session();

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->find($session->get('_user'));

        $logAction = $this->entityManager->getRepository(ActionForLog::class)->findOneBy([
            "name" => $action
        ]);
        if (!$logAction) {
            $logAction = new ActionForLog();
            $logAction->setName($action);
        }
        $loggin = new \App\Entity\Logger();
        $loggin->setIdUser($user);
        $loggin->setIdAction($logAction);
        $loggin->setInfo($info);

        $this->entityManager->persist($loggin);
        $this->entityManager->flush();
    }
}