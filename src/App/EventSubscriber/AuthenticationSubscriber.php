<?php


namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class AuthenticationSubscriber implements EventSubscriberInterface
{
    private $dispatcher;
    private $router;
    private $security;

    public function __construct(AuthorizationCheckerInterface $security = null, EventDispatcherInterface $dispatcher = null, RouterInterface $router = null) {
        $this->dispatcher = $dispatcher;
        $this->router = $router;
        $this->security = $security;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event) {
        $this->dispatcher->addListener(KernelEvents::RESPONSE, array($this, 'onKernelResponse'));
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $response = new RedirectResponse($this->router->generate('admin_home_index'));
        }else if( $this->security->isGranted('ROLE_MODERATOR')){
            $response = new RedirectResponse($this->router->generate('admin_users'));
        }
        else if( $this->security->isGranted('ROLE_CLIENT') or $this->security->isGranted('ROLE_DRIVER') ){
            $response = new RedirectResponse($this->router->generate('homepage'));
        }
        else{
            $response = new RedirectResponse($this->router->generate('homepage'));
        }

        $event->setResponse($response);
    }
    /**
     * Возвращает массив событий, которые необходимо слушать.
     *
     * @return array Имен событий
     */
    public static function getSubscribedEvents()
    {
        return array(
            SecurityEvents::INTERACTIVE_LOGIN=> 'onSecurityInteractiveLogin',
        );
    }

}