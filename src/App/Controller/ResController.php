<?php


namespace App\Controller;

use App\Entity\Adres;
use App\Entity\Guests;
use App\Entity\Restoran;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Tovar;
use App\Entity\User;
use App\Entity\UserAdres;
use App\Entity\Zakaz;
use App\Form\AdresType;
use App\Form\GuestsType;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;


class ResController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        // Avoid calling getUser() in the constructor: auth may not
        // be complete yet. Instead, store the entire Security object.
        $this->security = $security;
    }
    /**
     * @Route("/restoran/{id}", name="restoran_homepage")
     * @param $id
     * @return Response
     */
    public function restoran($id)
    {
//        $this->logger->addLog(self::LOG_VIEWONE, self::LOG_VIEWONE . ' Логин: '.$permission->getName() .' id: '. $permission->getId());
        $id_tip =$this->get('session')->get('id_tip');
        if ($id_tip == null || $id_tip == 1) {
            $tovar = $this->getDoctrine()->getRepository(Tovar::class)->findres($id);
        }
        else {
            $tovar = $this->getDoctrine()->getRepository(Tovar::class)->findAllInThisTip($id_tip);
        }

        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->find($id);
        $session_us = $this->getUser();
        $products = $this->get('session')->get('products');
        $sum = 0;
        if ($products) {
            foreach ($products as $item) {
                $sum += $item['summ'];
            }
        }
        return $this->render('tovar/restoran.html.twig', array(
            'restorans' => $restoran,
            'tovars' => $tovar,
            'session_u' => $session_us,
            'res'=>$id,
            'basket' => $this->renderView('orders/card.html.twig', array('products' => $products, 'sum' => $sum,'res'=>$id))
        ));
    }


    /**
     * @Route("/order", name="order")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function orderAction(Request $request)
    {
        $features = $this->get('session')->get('products');
//        dump($features);
        $restoran = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->findAll();
        $status = $this->getDoctrine()->getManager()->getRepository(Status::class)->find(1);
        $order = new Adres();
        $zakaz = new Zakaz();
        $guest = new Guests();
        $useradres = new UserAdres();
        /** @var User $user */
        $user = $this->security->getUser();
        if ($user){
            /** @var UserAdres $usadres */
            $usadres =$this->getDoctrine()->getManager()->getRepository(UserAdres::class)->findUsadresTip($user->getId());
        }
        $usadres = $useradres;
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', strtotime('now'));
//        dump(new \DateTime());

        /** @var User $user */
        $user = $this->security->getUser();

        $form = $this->createForm(AdresType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
                foreach ($features as $feature) {
                    $f = $this->getDoctrine()->getRepository(Tovar::class)->find($feature['feature']->getId());
                }
                $guestname = $form->get('guestname')->getData();
                $guestphone = $form->get('guestphone')->getData();
                $guest->setGuestname($guestname);
                $guest->setPhone($guestphone);
                $guestt = $this->getDoctrine()->getManager();
                $guestt->persist($guest);
                $guestt->flush();
                dump($guest);

                $orderr = $this->getDoctrine()->getManager();
                $orderr->persist($order);
                $orderr->flush();

                $useradres->setIdAdres($order);
                if ($user){
                    $useradres->setIdUser($user);
                }else{
                    $useradres->setIdGuest($guest);
                    $user = $guest;
                }
                $useradress = $this->getDoctrine()->getManager();
                $useradress->persist($useradres);
                $useradress->flush();

                $zakaz->setSumma($f->getCena());
                $zakaz->setStatus($status);
                $zakaz->setIdTovar($f);
                $zakaz->setDate(new \DateTime());
                $zakaz->setVremydostavki(new \DateTime());
                $zakaz->setIdUseradres($useradres);
                $zakazz = $this->getDoctrine()->getManager();
                $zakazz->persist($zakaz);
                $zakazz->flush();


                $this->get('session')->clear();
                $this->get('session')->remove('adres');
                $this->get('session')->set('adres', $order);

                $cookie = new Cookie('zakaz',$zakaz->getId(),time() + ( 2 * 365 * 24 * 60 * 60));
                $zak = new Response();
                $zak->headers->setCookie( $cookie );
                $zak->send();
                $cookieuser = new Cookie('user',$user->getId(),time() + ( 2 * 365 * 24 * 60 * 60));
                $zak = new Response();
                $zak->headers->setCookie($cookieuser);
                $zak->send();

                return $this->redirectToRoute('successzakaz/');

//            return $this->render('succeszakaz/success.html.twig', [
//                'adres' =>$order,
////                'zakaz' =>$zakaz,
//                'user' =>$user,
//                'restorans' => $restoran,
//                'features' => $features,
//            ]);
        }
        $products = $this->get('session')->get('products');
        $sum = 0;
        if ($products) {
            foreach ($products as $item) {
                $sum += $item['summ'];
            }
        }
        return $this->render('orders/order.html.twig', [
            'adress' =>$usadres,
            'restorans' => $restoran,
            'features' => $features,
            'products' => $products,
            'sum' => $sum,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}{per}{res}", name="deleteOrder", requirements={"id"="\d+","per"="\d+"})
     * @param $id
     * @param $per
     * @param $res
     * @return Response
     */
    public function deleteAction($id, $per, $res)
    {
        $features = $this->get('session')->get('products');
        $chet = $this->get('session')->get('chet');
        $products = [];
        foreach ($features as $feature) {
            if ($feature['feature']->getId() != $id) {
                array_push($products, $feature);
            }
        }
        $this->get('session')->clear();
        $this->get('session')->set('products', $products);
        $chet = 0;
        foreach ($products as $item) {
            $chet += $item['count'];

        }
        $this->get('session')->remove('chet');
        $this->get('session')->set('chet', $chet);
        if ($per == 0){
            return $this->redirectToRoute('restoran_homepage',['id'=>$res]);
        }
        if ($per == 1){
            return $this->redirectToRoute('order');
        }
    }

    /**
     * @Route("/basket/{res}", name="basket", methods={"GET|POST"})
     * @param Request $request
     * @param $res
     * @return JsonResponse|Response
     */
    public function basket(Request $request,$res)
    {
        $id_feature = $request->get('feature_id');
        $id_restoran = $request->get('restoran_id');
        $products = $this->get('session')->get('products');
        $chet = $this->get('session')->get('chet');
        if ($id_feature) {
            $feature = $this->getDoctrine()->getRepository(Tovar::class)->find($id_feature);

            $sum = 0;
            $chet = 0;
            if (!$feature) {
                return new JsonResponse('Товар не найден.', 301);
            }
            if (!$products)
                $products = [];
            $flag = false;
            /** @var Tovar $item */
            foreach ($products as $item) {
                $resname = $this->getDoctrine()->getRepository(Restoran::class)->find($item['feature']->getRestoran()->getId());
                if ($item['feature']->getId() == $feature->getId()) {
                    $flag = true;
//                    foreach ($item['feature']->getRestoran() as $itemm) {
//                        dump($itemm->getName());
//                    }
                }
                else{
                    dump($resname);
//                    $resname  = $res->getName();
                    return new Response($this->renderView('owibkares/owibkares.html.twig', [
                        'products' => $products,
                        'sum' => $sum,
                        'chet' => $chet,
                        'res' => $res,
                        'resname' => $resname->getName(),
                    ]));
                }
            }

            if (!$flag) {
                array_push($products, [
                    'feature' => $feature,
                    'count' => 1,
                    'summ' => $feature->getCena(),
                ]);

            } else {
                $prod = [];
                foreach ($products as $item) {
                    if ($item['feature']->getId() == $feature->getId()) {
                        $item['count']++;
                        $item['summ'] = $item['count'] * $feature->getCena();
                    }
                    array_push($prod, $item);
                }
                $products = $prod;
            }

            foreach ($products as $item) {
                $sum += $item['summ'];
                $chet += $item['count'];

            }
            $this->get('session')->remove('products');
            $this->get('session')->set('products', $products);
            $this->get('session')->remove('chet');
            $this->get('session')->set('chet', $chet);
        }
        return new Response($this->renderView('orders/card.html.twig', [
            'products' => $products,
            'sum' => $sum,
            'chet' => $chet,
            'res' => $res,
        ]));
    }

    /**
     * @Route("/minusOrder/{id}{per}{res}", name="minusOrder", requirements={"id"="\d+","per"="\d+"})
     * @param $id
     * @param $per
     * @param $res
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function minusOrder($id, $per,$res)
    {
        $features = $this->get('session')->get('products');
        $chet = $this->get('session')->get('chet');
        $products = [];
        foreach ($features as $feature) {
            if ($feature['feature']->getId() == $id) {
                $prod=$products;
                if ($feature['count'] != 1) {
                    $feature['count']--;
                    $feature['summ'] = $feature['count'] * $feature['feature']->getCena();
                    array_push($prod, $feature);
                }
                $products =$prod;
            }
            else{
                array_push($products, $feature);
            }
        }
        $this->get('session')->clear();
        $this->get('session')->set('products', $products);
        $chet = 0;
        foreach ($products as $item) {
            $chet += $item['count'];
        }
        $this->get('session')->remove('chet');
        $this->get('session')->set('chet', $chet);
        if ($per == 0){
            return $this->redirectToRoute('restoran_homepage',['id'=>$res]);
        }
        if ($per == 1){
            return $this->redirectToRoute('order');
        }
    }
    /**
     * @Route("/plusOrder/{id}{per}{res}", name="plusOrder", requirements={"id"="\d+","per"="\d+"})
     * @param $id
     * @param $per
     * @param $res
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function plusOrder($id, $per,$res)
    {
        $features = $this->get('session')->get('products');
        $chet = $this->get('session')->get('chet');
        $products = [];
        foreach ($features as $feature) {
            if ($feature['feature']->getId() == $id) {
                $feature['count']++;
                $feature['summ'] = $feature['count'] * $feature['feature']->getCena();
            }
            array_push($products, $feature);

        }
        $this->get('session')->clear();
        $this->get('session')->set('products', $products);
        $chet = 0;
        foreach ($products as $item) {
            $chet += $item['count'];

        }
        $this->get('session')->remove('chet');
        $this->get('session')->set('chet', $chet);
        if ($per == 0){
            return $this->redirectToRoute('restoran_homepage',['id'=>$res]);
        }
        if ($per == 1){
            return $this->redirectToRoute('order');
        }
    }

    /**
     * @Route("/clear", name="clear", methods={"GET|POST"})
     */
    public function clear()
    {
        $this->get('session')->clear();
        return $this->redirectToRoute('homepage');
    }


}