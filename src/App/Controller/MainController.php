<?php


namespace App\Controller;

use App\Entity\Restoran;
use App\Entity\Tip;
use App\Entity\Tovar;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MainController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function homepage(Request $request)
    {
//        $this->logger->addLog(self::LOG_VIEWONE, self::LOG_VIEWONE . ' Логин: '.$permission->getName() .' id: '. $permission->getId());
        $id_tip = $this->get('session')->get('id_tip');
        if ($id_tip == null || $id_tip == 1) {
            $restoran = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->findAll();
        }
        else{
            $restoran = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->findAllRes($id_tip);
        }
        $tips = $this->getDoctrine()->getManager()->getRepository(Tip::class)->findAll();
        $session_us = $this->getUser();

        $this->get('session')->set('id_tip', $id_tip);
        return $this->render('base.html.twig', [
            'restorans' => $restoran,
            'session_u' => $session_us,
//            'tovars' => $tovar,
            'tips' => $tips,
            'res' => 5,
            'blocksart' => $this->renderView('tovar/blocksart.html.twig', [
//                'tovars' => $tovar,
                'tips' => $tips,
                'restorans' => $restoran])
        ]);
    }

    /**
     * @Route("/getBlockSart", name="blocksart_homepage", methods={"GET|POST"})
     * @param Request $request
     * @return Response
     */
    public function blocksart(Request $request)
    {
        $id_tip = $request->get('tip_id');
        $this->get('session')->set('id_tip', $id_tip);
        if ($id_tip == null || $id_tip == 1) {
            $restorans = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->findAll();
        }
        else {
            $restorans = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->findAllRes($id_tip);
        }
        return new Response($this->renderView('tovar/blocksart.html.twig', [
//            'tovars' => $tovars,
            'restorans' => $restorans,
        ]));
    }
    /**
     * @Route("/livesearch", name="livesearch")
     * @param Request $request
     * @return Response
     */
    public function LiveSearchTip(Request $request)
    {
        $referal = $request->get('referal');
        dump($referal);
        $tovar = $this->getDoctrine()->getManager()->getRepository(Tovar::class)->livesearch($referal);
        dump($tovar);
        $restorans = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->find(5);
        $products = $this->get('session')->get('products');
        $sum = 0;
        if ($products) {
            foreach ($products as $item) {
                $sum += $item['summ'];
            }
        }
        $id = 5;
        return $this->render('search/livesearch.html.twig', array(
            'tovars' => $tovar,
            'res'=>$id,
            'restorans' => $restorans,
            'basket' => $this->renderView('orders/card.html.twig', array('products' => $products, 'sum' => $sum,'res'=>$id))
        ));
    }
    /**
     * @Route("/productmodlaka", name="productmodlaka")
     * @param Request $request
     * @return Response
     */
    public function ProductModlakaTip(Request $request)
    {
        $referal = $request->get('referal');
        dump($referal);
        $tovar = $this->getDoctrine()->getManager()->getRepository(Tovar::class)->searchformodalka($referal);
        dump($tovar);
        $restorans = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->find(5);
        $products = $this->get('session')->get('products');
        $sum = 0;
        $chet = 0;
        if ($products) {
            foreach ($products as $item) {
                $sum += $item['summ'];
                $chet += $item['count'];
            }
        }
        $this->get('session')->remove('products');
        $this->get('session')->set('products', $products);
        $this->get('session')->remove('chet');
        $this->get('session')->set('chet', $chet);
        $id = 5;
        return $this->render('modalka/modalka.html.twig', array(
            'tovars' => $tovar,
            'res'=>$id,
            'restorans' => $restorans,
            'basket' => $this->renderView('orders/card.html.twig', array('products' => $products, 'sum' => $sum,'res'=>$id))
        ));
    }

    /**
     * @Route("/search", name="search")
     * @param Request $request
     * @return Response
     */
    public function SearchTip(Request $request)
    {
        $request->request->get('type');
        $referal = $request->get('referal');
        dump($request);
        dump($referal);
        $tovar = $this->getDoctrine()->getManager()->getRepository(Tovar::class)->searchTip($referal);

        foreach ($tovar as $item){
            dump($item->getRestoran()->getId());
            $idres = $item->getRestoran()->getId();
            $restorans = $this->getDoctrine()->getManager()->getRepository(Restoran::class)->find($idres);
            dump($restorans);
        }

        $products = $this->get('session')->get('products');
        $sum = 0;
        if ($products) {
            foreach ($products as $item) {
                $sum += $item['summ'];
            }
        }
        $id = 5;
        $isAjax = $request->isXMLHttpRequest();
        dump($isAjax);
        return $this->render('search.html.twig', array(
            'tovars' => $tovar,
            'res'=>$id,
            'restorans' => $restorans,
            'basket' => $this->renderView('orders/card.html.twig', array('products' => $products, 'sum' => $sum,'res'=>$id))
        ));
    }
}