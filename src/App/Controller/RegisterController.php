<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Request $request
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function register(Request $request,Security $security)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }
        $user = new User();
        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'isAdmin'=> $this->isGranted("ROLE_ADMIN") ? true : false
            ]
        );


        $encoder = new \Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder(12);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $user->setSalt(time());
            $password = $user->getPassword();
            $user->setPassword($encoder->encodePassword($password, $user->getSalt()));


            if ($this->isGranted("ROLE_ADMIN")){
                 foreach ($form->get('userRoles')->getData() as $item){
                    $user->addUserRole($item);
                 }
            }
            else{
                $role = $this->getDoctrine()->getRepository(Role::class)->findOneBy(array('name' => "ROLE_USER"));
                $user->addUserRole($role);
            }


            //$user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

//            return $this->redirectToRoute('login');
        }
        $entityManager = $this->getDoctrine()->getManager();
        $userr = $entityManager->getRepository(User::class)->findAll();
        $roles = $this->getDoctrine()->getManager()->getRepository(Role::class)->findAll();

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
            'users' => $userr,
            'roles' => $roles,
        ]);
}


}
