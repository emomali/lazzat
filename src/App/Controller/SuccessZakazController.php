<?php


namespace App\Controller;

use App\Entity\Guests;
use App\Entity\Restoran;
use App\Entity\Status;
use App\Entity\Tovar;
use App\Entity\User;
use App\Entity\UserAdres;
use App\Entity\Zakaz;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class SuccessZakazController
 * @package App\Controller
 * @Route("/succeszakaz", name="successzakaz")
 */
class SuccessZakazController extends AbstractController
{
    private $security;

    /**
     * @Route("/", name="/")
     * @param Request $request
     * @return Response
     */
    public function success(Request $request)
    {

        $cookies = $request->cookies;
        $zakazid = $cookies->get('zakaz');
        $userid = $cookies->get('user');
        $zakaz = $this->getDoctrine()->getManager()->getRepository(Zakaz::class)->find($zakazid);
        $useradres = $this->getDoctrine()->getManager()->getRepository(UserAdres::class)->find($zakaz->getIdUseradres());
        if (!$this->isGranted(['IS_AUTHENTICATED_ANONYMOUSLY'])) {
            $user = $this->getDoctrine()->getManager()->getRepository(User::class)->find($userid);
            dump('user');
        } else {
            $user = $this->getDoctrine()->getManager()->getRepository(Guests::class)->find($userid);
            dump('guest');
        }
        dump($zakaz);
        dump($useradres->getIdGuest());

        $cookie = new Cookie('zakaz', $zakaz->getId(), time() + (2 * 365 * 24 * 60 * 60));
        $zak = new Response();
        $zak->headers->setCookie($cookie);
        $zak->send();

        return $this->render('succeszakaz/success.html.twig', [
//            'adres' =>$order,
////                'zakaz' =>$zakaz,
//            'user' =>$user,
//            'restorans' => $restoran,
//            'features' => $features,
            'cookies' => $cookies,
            'zakaz' => $zakaz,
            'user' => $useradres,
            'progress' => $this->renderView('orders/progress.html.twig', [
                'cookies' => $cookies,
                'zakaz' => $zakaz,
                'user' => $useradres])
        ]);

    }

    /**
     * @Route("/obrobatka", name="_obrabotka")
     * @param Request $request
     * @return Response
     */
    public function obrobatka(Request $request)
    {
        $cookies = $request->cookies;
        $zakazid = $cookies->get('zakaz');
        $zakaz = $this->getDoctrine()->getManager()->getRepository(Zakaz::class)->find($zakazid);
        $useradres = $this->getDoctrine()->getManager()->getRepository(UserAdres::class)->find($zakaz->getIdUseradres());

//        $statusidt = $request->get('statusidt');
//        dump($statusidt);
//        $statusidt = $statusidt + 1;
//        $status = $this->getDoctrine()->getManager()->getRepository(Status::class)->find($statusidt);
//
//        $zakaz->setStatus($status);
//        $zakazz = $this->getDoctrine()->getManager();
//        $zakazz->persist($zakaz);
//        $zakazz->flush();
//
//        dump($zakaz);


        return $this->render('orders/progress.html.twig', [
//            'adres' =>$order,
////                'zakaz' =>$zakaz,
//            'user' =>$user,
//            'restorans' => $restoran,
//            'features' => $features,
            'cookies' => $cookies,
            'zakaz' => $zakaz,
            'user' => $useradres,
        ]);

    }
}