<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Sensio\Bundle\FrameworkExtraBundle\DependencyInjection\Compiler\AddExpressionLanguageProvidersPass;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Restoran
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="categorycharacteristic", options={"comment":"Таблица ресторанов"})
 */

class CategoryCharacteristic
{
    /**
     * @var int идентификатор ресторана
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_categorycharacteristic", type="integer", unique=true, options={"comment":"ИД ресторана"})
     */
    private $id;
    /**
     * @var Characteristic
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Characteristic")
     * @ORM\JoinColumn(name="id_characteristic", referencedColumnName="id_characteristic", nullable=false)
     */
    private $id_characteristic;
    /**
     * @var ActionForLog идентификатор действия пользователя
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @ORM\JoinColumn(name="id_category", referencedColumnName="id_category")
     */
    private $id_category;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCharacteristic(): ?Characteristic
    {
        return $this->id_characteristic;
    }

    public function setIdCharacteristic(?Characteristic $id_characteristic): self
    {
        $this->id_characteristic = $id_characteristic;

        return $this;
    }

    public function getIdCategory(): ?Category
    {
        return $this->id_category;
    }

    public function setIdCategory(?Category $id_category): self
    {
        $this->id_category = $id_category;

        return $this;
    }


}