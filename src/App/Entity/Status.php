<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Status
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="status", options={"comment":"Статус заказа"})
 */

class Status
{
    /**
     * @var int идентификатор адреса
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_status", type="integer", unique=true, options={"comment":"ИД статуса"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="name", length=100, options={"comment":"Статус"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $name;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }


}