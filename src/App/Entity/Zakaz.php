<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Zakaz
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="zakaz", options={"comment":"Таблица заказов"})
 */

class Zakaz
{
    /**
     * @var int идентификатор ресторана
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_zakaz", type="integer", unique=true, options={"comment":"ИД ресторана"})
     */
    private $id;
    /**
     * @var Tovar
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Tovar")
     * @ORM\JoinColumn(name="id_tovar", referencedColumnName="id_tovar", nullable=false)
     */
    private $id_tovar;
    /**
     * @var UserAdres
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\UserAdres")
     * @ORM\JoinColumn(name="id_useradres", referencedColumnName="id_useradres", nullable=false)
     */
    private $id_useradres;

    /**
     * @var int
     * @ORM\Column(name="summa", type="integer", options={"comment":"Оценка ресторана"})
     */
    private $summa;

    /**
     * @ORM\Column(name="date", type="date",  options={"comment":"Оценка ресторана"})
     */
    private $date;

    /**
     * @ORM\Column(name="vremydostavki", type="datetime", options={"comment":"Оценка ресторана"})
     */
    private $vremydostavki;

    /**
     * @var Status
     * @ORM\ManyToOne(targetEntity="App\Entity\Status")
     * @ORM\JoinColumn(name="id_status", referencedColumnName="id_status")
     */
    private $status;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }



    /**
     * @return Tovar
     */
    public function getIdTovar(): Tovar
    {
        return $this->id_tovar;
    }

    /**
     * @param Tovar $id_tovar
     */
    public function setIdTovar(Tovar $id_tovar)
    {
        $this->id_tovar = $id_tovar;
    }

    /**
     * @return int
     */
    public function getSumma()
    {
        return $this->summa;
    }

    /**
     * @param int $summa
     */
    public function setSumma(int $summa)
    {
        $this->summa = $summa;
    }

    /**
     * @return UserAdres
     */
    public function getIdUseradres()
    {
        return $this->id_useradres;
    }

    /**
     * @param UserAdres $id_useradres
     */
    public function setIdUseradres(UserAdres $id_useradres)
    {
        $this->id_useradres = $id_useradres;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date)
    {
        $this->date = $date;

        return $this;
    }

    public function getVremydostavki(): ?\DateTimeInterface
    {
        return $this->vremydostavki;
    }

    public function setVremydostavki(\DateTimeInterface $vremydostavki)
    {
        $this->vremydostavki = $vremydostavki;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(?Status $status)
    {
        $this->status = $status;

        return $this;
    }

}