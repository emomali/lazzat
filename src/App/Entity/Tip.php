<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Tip
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LoaderTipRepository")
 * @ORM\Entity()
 * @ORM\Table(name="tip", options={"comment":"Таблица тип-товара"})
 */

class Tip
{
    /**
     * @var int идентификатор ресторана
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_tip", type="integer", unique=true, options={"comment":"ИД типа"})
     */
    private $id;

    /**
     * @var string название ресторана
     *
     * @ORM\Column(name="name", type="string", options={"comment":"Название типа"})
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var ArrayCollection Права доступа принадлежащие ролям
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tovar", inversedBy="tip")
     * @ORM\JoinTable(name="tip_tovars",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_tip", referencedColumnName="id_tip")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_tovar", referencedColumnName="id_tovar")
     *  }
     * )
     */
    private $tovars;

    public function __construct()
    {
        $this->tovars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Tovar[]
     */
    public function getTovars(): Collection
    {
        return $this->tovars;
    }

    public function addTovar(Tovar $tovar): self
    {
        if (!$this->tovars->contains($tovar)) {
            $this->tovars[] = $tovar;
        }

        return $this;
    }

    public function removeTovar(Tovar $tovar): self
    {
        if ($this->tovars->contains($tovar)) {
            $this->tovars->removeElement($tovar);
        }

        return $this;
    }

}
