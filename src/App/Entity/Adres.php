<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Adres
 *
 * @package App\Entity *
 * @ORM\Entity(repositoryClass="App\Repository\AdresRepository")
 * @ORM\Table(name="adres", options={"comment":"Таблица адреса"})
 */

class Adres
{
    /**
     * @var int идентификатор адреса
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_adres", type="integer", unique=true, options={"comment":"ИД адрес"})
     */
    private $id;

    /**
     * @var string Годор
     *
     * @ORM\Column(name="city", type="string", options={"comment":"Название город"})
     * @Assert\NotBlank()
     */
    private $city;
    /**
     * @var string Улица
     *
     * @ORM\Column(name="street", type="string", options={"comment":"Название улицы"})
     * @Assert\NotBlank()
     */
    private $street;



    /**
     * @var int Дом
     *
     * @ORM\Column(name="house", type="integer", options={"comment":"Номер дома"})
     * @Assert\NotBlank()
     */
    private $house;
    /**
     * @var int Подъезд
     *
     * @ORM\Column(name="porch", type="integer", options={"comment":"Подъезд"})
     * @Assert\NotBlank()
     */
    private $porch;
    /**
     * @var int Квартира
     *
     * @ORM\Column(name="apartment", type="integer", options={"comment":"Квартира"})
     * @Assert\NotBlank()
     */
    private $apartment;


    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Restoran", mappedBy="adress")
     */
    private $restorans;

    public function __construct()
    {
        $this->restorans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouse(): ?int
    {
        return $this->house;
    }

    public function setHouse(int $house): self
    {
        $this->house = $house;

        return $this;
    }

    public function getPorch(): ?int
    {
        return $this->porch;
    }

    public function setPorch(int $porch): self
    {
        $this->porch = $porch;

        return $this;
    }

    public function getApartment(): ?int
    {
        return $this->apartment;
    }

    public function setApartment(int $apartment): self
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * @return Collection|Restoran[]
     */
    public function getRestorans(): Collection
    {
        return $this->restorans;
    }

    public function addRestoran(Restoran $restoran)
    {
        if (!$this->restorans->contains($restoran)) {
            $this->restorans[] = $restoran;
            $restoran->addAdre($this);
        }

        return $this;
    }

    public function removeRestoran(Restoran $restoran)
    {
        if ($this->restorans->contains($restoran)) {
            $this->restorans->removeElement($restoran);
            $restoran->removeAdre($this);
        }

        return $this;
    }



}