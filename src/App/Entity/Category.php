<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Adres
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="category", options={"comment":"Таблица адреса"})
 */

class Category
{
    /**
     * @var int идентификатор адреса
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_category", type="integer", unique=true, options={"comment":"ИД адрес"})
     */
    private $id;

    /**
     * @var string название ресторана
     *
     * @ORM\Column(name="name", type="string", unique=true, options={"comment":"Название характеристики"})
     * @Assert\NotBlank()
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


}