<?php


namespace App\Entity;


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * Логирование
 *
 *
 * Class Logger
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LoggerRepository")
 * @ORM\Entity()
 * @ORM\Table(name="logger", options={"comment":"Таблица  логирования"})
 */
class Logger implements JsonSerializable
{
    /**
     * @var int идентификатор лога
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", unique=true, options={"comment":"Идентификатор лога"})
     */
    private $id;


    /**
     * @var string Системное имя права доступа
     *
     * @ORM\Column(type="string", name="info", unique=false, options={"comment":"Информация лога"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $info;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=false)
     */
    private $id_user;


    /**
     * @var ActionForLog идентификатор действия пользователя
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ActionForLog", cascade={"persist"})
     * @ORM\JoinColumn(name="id_action", referencedColumnName="id_action")
     */
    private $id_action;


    /**
     * @var DateTime
     * @ORM\Column(name="data_this_log", type="datetime", nullable=false, options={"comment":"Дата добовления лога"})
     */
    private $data_this_log;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->data_this_log = new \DateTime('now');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return Logger
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return Logger
     */
    public function setInfo($info)
    {
        $this->info = $info;
        return $this;
    }

    /**
     * @return User
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param $user
     * @return Logger
     */
    public function setIdUser($user)
    {
        $this->id_user = $user;
        return $this;
    }

    /**
     * @return ActionForLog
     */
    public function getIdAction()
    {
        return $this->id_action;
    }

    /**
     * @param ActionForLog $id_action
     * @return Logger
     */
    public function setIdAction(ActionForLog $id_action)
    {
        $this->id_action = $id_action;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataThisLog()
    {
        return $this->data_this_log;
    }

    /**
     * @param DateTime $data_this_log
     * @return Logger
     */
    public function setDataThisLog(DateTime $data_this_log)
    {
        $this->data_this_log = $data_this_log;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'info' => $this->info,
            'id_user' => $this->id_user,
            'id_action' => $this->id_action,
            'data_this_log' => date_format($this->data_this_log, 'd.m.Y H:m:s')

        );
    }
}
