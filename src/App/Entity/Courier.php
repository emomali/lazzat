<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use JsonSerializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class User
 *
 * @package App\Entity
 *
 * @ORM\Table(name="courier", options={"comment":"Таблица пользователей"})
 * @ORM\Entity
 */

class Courier
{
    /**
     * @var int Идентификатор пользователя
     *
     * @ORM\Id
     * @ORM\Column(name="id_courier", type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string имя пользователя портала
     *
     * @ORM\Column(name="username", type="string", length=50, unique=true, options={"comment":"Имя пользователя"})
     * @Assert\Regex(
     *     pattern="/^[0-9A-Za-z-]+$/",
     *     message="Имя пользователя может содержать только A-z, цифры 0-9 и тире"
     * )
     * @Assert\NotBlank(message="Поле не может быть пустым")
     * @Assert\Length(
     *      min = 4,
     *      max = 50,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $username;

    /**
     * @var string фамилия пользователя
     *
     * @ORM\Column(name="surname", type="string", length=60,  options={"comment":"Фамилия пользователя"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      max = 60,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $surname;

    /**
     * @var string имя пользователя
     *
     * @ORM\Column(name="name", type="string", length=50, options={"comment":"Имя пользователя"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      max = 50,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $name;
    /**
     * @var string фамилия пользователя
     *
     * @ORM\Column(name="midlename", type="string", length=60, nullable=true,  options={"comment":"Фамилия пользователя"})
     * @Assert\Length(
     *      max = 60,
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $midlename;

    /**
     * @var string Телефон пользователя
     *
     * @ORM\Column(name="phone", type="string", length=12, options={"comment":"Телефон пользователя"})
     * @Assert\Length(
     *     min=5,
     *     max=12,
     *     minMessage="Введен не корректный номер",
     *     maxMessage="Веден не корректный номер")
     */
    private $phone;
    /**
     * @var string фамилия пользователя
     *
     * @ORM\Column(name="adres", type="string",  options={"comment":"Адрес курьера"})
     * @Assert\NotBlank()
     *
     */
    private $adres;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, options={"comment":"Пароль пользователя"})
     * @Assert\Length(min=6, minMessage="Должно быть не менее {{ limit }} символов")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", options={"comment":"Соль пароля"})
     */
    private $salt;

    /**
     * @var string Email-адрес пользователя
     *
     * @ORM\Column(name="email", type="string", unique=true, options={"comment":"Email-адрес пользователя"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     * @Assert\Email(message="Недопустимый электронный адрес")
     */
    private $email;

    /**
     * Many features have one product. This is the owning side.
     * @ManyToOne(targetEntity="Role", inversedBy="courier")
     * @JoinColumn(name="id_role", referencedColumnName="id_role")
     */
    private $role;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMidlename(): ?string
    {
        return $this->midlename;
    }

    public function setMidlename(?string $midlename): self
    {
        $this->midlename = $midlename;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAdres(): ?string
    {
        return $this->adres;
    }

    public function setAdres(string $adres): self
    {
        $this->adres = $adres;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }


}