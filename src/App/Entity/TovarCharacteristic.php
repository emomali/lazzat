<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Sensio\Bundle\FrameworkExtraBundle\DependencyInjection\Compiler\AddExpressionLanguageProvidersPass;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Restoran
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="tovarcharacteristic", options={"comment":"Таблица ресторанов"})
 */

class TovarCharacteristic
{
    /**
     * @var int идентификатор ресторана
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_tovarcharacteristic", type="integer", unique=true, options={"comment":"ИД ресторана"})
     */
    private $id;
    /**
     * @var Tovar
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Tovar")
     * @ORM\JoinColumn(name="id_tovar", referencedColumnName="id_tovar", nullable=false)
     */
    private $id_tovar;
    /**
     * @var ActionForLog идентификатор действия пользователя
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryCharacteristic")
     * @ORM\JoinColumn(name="id_categorycharacteristic", referencedColumnName="id_categorycharacteristic")
     */
    private $id_categorycharacteristic;
    /**
     * @var string название ресторана
     *
     * @ORM\Column(name="value", type="string", unique=true, options={"comment":"Название характеристики"})
     * @Assert\NotBlank()
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getIdTovar(): ?Tovar
    {
        return $this->id_tovar;
    }

    public function setIdTovar(?Tovar $id_tovar): self
    {
        $this->id_tovar = $id_tovar;

        return $this;
    }

    public function getIdCategorycharacteristic(): ?CategoryCharacteristic
    {
        return $this->id_categorycharacteristic;
    }

    public function setIdCategorycharacteristic(?CategoryCharacteristic $id_categorycharacteristic): self
    {
        $this->id_categorycharacteristic = $id_categorycharacteristic;

        return $this;
    }

}