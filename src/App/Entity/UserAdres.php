<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Sensio\Bundle\FrameworkExtraBundle\DependencyInjection\Compiler\AddExpressionLanguageProvidersPass;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Restoran
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\UsadresRepository")
 * @ORM\Table(name="useradres", options={"comment":"Таблица ресторанов"})
 */

class UserAdres
{
    /**
     * @var int идентификатор ресторана
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_useradres", type="integer", unique=true, options={"comment":"ИД ресторана"})
     */
    private $id;
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id_user", nullable=true)
     */
    private $id_user;
    /**
     * @var Guests
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Guests")
     * @ORM\JoinColumn(name="id_guest", referencedColumnName="id_guest", nullable=true)
     */
    private $id_guest;
    /**
     * @var Adres идентификатор действия пользователя
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Adres")
     * @ORM\JoinColumn(name="id_adres", referencedColumnName="id_adres")
     */
    private $id_adres;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param User $id_user
     */
    public function setIdUser(User $id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return Adres
     */
    public function getIdAdres()
    {
        return $this->id_adres;
    }

    /**
     * @param Adres $id_adres
     */
    public function setIdAdres(Adres $id_adres)
    {
        $this->id_adres = $id_adres;
    }

    public function getIdGuest()
    {
        return $this->id_guest;
    }

    public function setIdGuest(?Guests $id_guest)
    {
        $this->id_guest = $id_guest;

        return $this;
    }

}