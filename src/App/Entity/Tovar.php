<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;


/**
 *
 * Class Tovar
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LoaderTipRepository")
 * @ORM\Table(name="tovar", options={"comment":"Таблица товаров"})
 */

class Tovar
{
    /**
     * @var int идентификатор товара
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_tovar", type="integer", unique=true, options={"comment":"Идентификатор товара"})
     */
    private $id;

    /**
     * @var string Название товара
     *
     * @ORM\Column(type="string", name="name", length=100, options={"comment":"Название товара"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $name;
    /**
     * @var Restoran
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Restoran")
     * @ORM\JoinColumn(name="id_restoran", referencedColumnName="id_restoran", nullable=true)
     */
    private $restoran;
    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tip", mappedBy="tovars")
     */
    private $tip;
    /**
     * @var ActionForLog идентификатор действия пользователя
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @ORM\JoinColumn(name="id_category", referencedColumnName="id_category")
     */
    private $id_category;
    /**
     * @var int оценка ресторана
     * @ORM\Column(name="cena", type="integer",  options={"comment":"Оценка ресторана"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $cena;

    /**
     * @var int оценка ресторана
     * @ORM\Column(name="kolichestvo", type="integer",  options={"comment":"Оценка ресторана"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $kolichestvo;

    /**
     * @var string Название товара
     *
     * @ORM\Column(type="string", name="coment", length=100,  options={"comment":"Название товара"})
     */
    private $coment;
    /**
     * @var string URL изображения
     *
     * @ORM\Column(name="imageLink", type="string", length=255, nullable=true, options={"comment":"Ссылка на изображение профиля"})

     */
    private $imageLink;

    public function __construct()
    {
        $this->tip = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCena(): ?int
    {
        return $this->cena;
    }

    public function setCena(int $cena): self
    {
        $this->cena = $cena;

        return $this;
    }

    public function getKolichestvo(): ?int
    {
        return $this->kolichestvo;
    }

    public function setKolichestvo(int $kolichestvo): self
    {
        $this->kolichestvo = $kolichestvo;

        return $this;
    }

    public function getComent(): ?string
    {
        return $this->coment;
    }

    public function setComent(string $coment): self
    {
        $this->coment = $coment;

        return $this;
    }

    public function getImageLink(): ?string
    {
        return $this->imageLink;
    }

    public function setImageLink(?string $imageLink): self
    {
        $this->imageLink = $imageLink;

        return $this;
    }

    public function getRestoran(): ?Restoran
    {
        return $this->restoran;
    }

    public function setRestoran(?Restoran $restoran): self
    {
        $this->restoran = $restoran;

        return $this;
    }

    /**
     * @return Collection|Tip[]
     */
    public function getTip(): Collection
    {
        return $this->tip;
    }

    public function addTip(Tip $tip): self
    {
        if (!$this->tip->contains($tip)) {
            $this->tip[] = $tip;
            $tip->addTovar($this);
        }

        return $this;
    }

    public function removeTip(Tip $tip): self
    {
        if ($this->tip->contains($tip)) {
            $this->tip->removeElement($tip);
            $tip->removeTovar($this);
        }

        return $this;
    }

    public function getIdCategory(): ?Category
    {
        return $this->id_category;
    }

    public function setIdCategory(?Category $id_category): self
    {
        $this->id_category = $id_category;

        return $this;
    }

}