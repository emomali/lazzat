<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class User
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users", options={"comment":"Таблица пользователей"})
 */

class User implements UserInterface
{
    /**
     * @var int Идентификатор пользователя
     *
     * @ORM\Id
     * @ORM\Column(name="id_user", type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string имя пользователя портала
     *
     * @ORM\Column(name="username", type="string", length=50, unique=true, options={"comment":"Имя пользователя"})
     * @Assert\Regex(
     *     pattern="/^[0-9A-Za-z-]+$/",
     *     message="Имя пользователя может содержать только A-z, цифры 0-9 и тире"
     * )
     * @Assert\NotBlank(message="Поле не может быть пустым")
     * @Assert\Length(
     *      min = 4,
     *      max = 50,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $username;

    /**
     * @var string фамилия пользователя
     *
     * @ORM\Column(name="surname", type="string", length=60,  options={"comment":"Фамилия пользователя"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      max = 60,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $surname;

    /**
     * @var string имя пользователя
     *
     * @ORM\Column(name="name", type="string", length=50, options={"comment":"Имя пользователя"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $name;
    /**
     * @var string Телефон пользователя
     *
     * @ORM\Column(name="phone", type="string", length=12, options={"comment":"Телефон пользователя"})
     * @Assert\Length(
     *     min=5,
     *     max=12,
     *     minMessage="Введен не корректный номер",
     *     maxMessage="Веден не корректный номер")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, options={"comment":"Пароль пользователя"})
     * @Assert\Length(min=6, minMessage="Должно быть не менее {{ limit }} символов")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", options={"comment":"Соль пароля"})
     */
    private $salt;

    /**
     * @var string Email-адрес пользователя
     *
     * @ORM\Column(name="email", type="string", unique=true, options={"comment":"Email-адрес пользователя"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     * @Assert\Email(message="Недопустимый электронный адрес")
     */
    private $email;

    /**
     * Роль пользователя
     *
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="users_roles",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id_user")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id_role")}
     * )
     */
    private $userRoles;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
    }



    /**
     * Add role.
     *
     * @param Role $role
     *
     * @return User
     */
    public function addUserRole(Role $role)
    {
        $this->userRoles[] = $role;

        return $this;
    }

    /**
     * Remove role.
     *
     * @param Role $role
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUserRole(Role $role)
    {
        return $this->userRoles->removeElement($role);
    }

    /**
     * Геттер для ролей пользователя.
     *
     * @return ArrayCollection A Doctrine ArrayCollection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return array (Role|string)[] The user roles
     */
    public function getRoles()
    {
        // Почему-то не работает
        //return $this->getUserRoles()->toArray();

        $roles = [];

        foreach ($this->getUserRoles() as $item) {
            array_push($roles, $item->getName());
        }

        return $roles;
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt.
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * @return string The surname
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }



    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }





}