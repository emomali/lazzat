<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class Guests
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="guests", options={"comment":"Таблица пользователей"})
 */

class Guests
{
    /**
     * @var int Идентификатор пользователя
     *
     * @ORM\Id
     * @ORM\Column(name="id_guest", type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string имя пользователя
     *
     * @ORM\Column(name="guestname", type="string", length=50, options={"comment":"Имя пользователя"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $guestname;

    /**
     * @var string Телефон пользователя
     *
     * @ORM\Column(name="phone", type="string", length=12, options={"comment":"Телефон пользователя"})
     * @Assert\Length(
     *     min=8,
     *     max=12,
     *     minMessage="Введен не корректный номер",
     *     maxMessage="Веден не корректный номер")
     */
    private $phone;

    public function getId()
    {
        return $this->id;
    }

    public function getGuestname()
    {
        return $this->guestname;
    }

    public function setGuestname(string $guestname)
    {
        $this->guestname = $guestname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone)
    {
        $this->phone = $phone;

        return $this;
    }



}