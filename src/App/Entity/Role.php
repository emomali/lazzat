<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="roles", options={"comment":"Таблица ролей"})
 */

class Role
{
    /**
     * @var int идентификатор роли
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_role", type="integer", unique=true, options={"comment":"ИД роли"})
     */
    private $id;

    /**
     * @var string название роли
     *
     * @ORM\Column(name="name", type="string", length=16, unique=true, options={"comment":"Название роли"})
     * @Assert\Length(
     *     min=5,
     *     max=25,
     *     minMessage="Должно быть не менее {{ limit }} символов",
     *     maxMessage="Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank()
     */
    private $name;


    /**
     * @var ArrayCollection Права доступа принадлежащие ролям
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Permission", inversedBy="roles")
     * @ORM\JoinTable(name="role_permissions",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_role", referencedColumnName="id_role")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_permission", referencedColumnName="id_permission")
     *  }
     * )
     */
    private $permissions;
    /**
     * One product has many features. This is the inverse side.
     * @OneToMany(targetEntity="Courier", mappedBy="role")
     */
    private $courier;

    /**
     * Constructor
     */
    public function __construct() {
        $this->permissions = new ArrayCollection();
        $this->courier = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }


    /**
     * @return Collection
     */
    public function getPermissions() {
        return $this->permissions;
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return array (Permission|string)[] The user roles
     */
    public function getPermission() {
        $permissions = [];

        foreach ($this->getPermissions() as $item) {
            array_push($permissions, $item->getName());
        }

        return $permissions;
    }

    /**
     * Добавить право доступа
     *
     * @param Permission $permission
     *
     * @return Role|array
     */
    public function addPermission(Permission $permission) {
        $this->permissions[] = $permission;

        return $this;
    }

    /**
     * Удалить право доступа
     *
     * @param Permission $permission
     */
    public function removePermission(Permission $permission) {
        $this->permissions->removeElement($permission);
    }

    /**
     * @return Collection|Courier[]
     */
    public function getCourier(): Collection
    {
        return $this->courier;
    }

    public function addCourier(Courier $courier): self
    {
        if (!$this->courier->contains($courier)) {
            $this->courier[] = $courier;
            $courier->setIdCourier($this);
        }

        return $this;
    }

    public function removeCourier(Courier $courier): self
    {
        if ($this->courier->contains($courier)) {
            $this->courier->removeElement($courier);
            // set the owning side to null (unless already changed)
            if ($courier->getIdCourier() === $this) {
                $courier->setIdCourier(null);
            }
        }

        return $this;
    }

}