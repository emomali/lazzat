<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ActionForLog
 *
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="action_for_log", options={"comment":"Таблица Справочник действий пользователя"})
 */
class ActionForLog implements \JsonSerializable
{
    /**
     * @var int идентификатор действия
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_action", type="integer", unique=true, options={"comment":"Идентификатор действия"})
     */
    private $id;

    /**
     * @var string Имя действия
     *
     * @ORM\Column(name="name",type="string", length=255, options={"comment":"Имя действия"})
     * @Assert\Length(max=60, maxMessage="Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $name;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void {
        $this->name = $name;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,

        );
    }
}