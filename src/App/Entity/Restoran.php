<?php


namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Restoran
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LoaderResRepository")
 * @ORM\Table(name="restoran", options={"comment":"Таблица ресторанов"})
 */

class Restoran
{
    /**
     * @var int идентификатор ресторана
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_restoran", type="integer", unique=true, options={"comment":"ИД ресторана"})
     */
    private $id;
    /**
     * @var string название ресторана
     * @ORM\Column(name="name", type="string", options={"comment":"Название рестрона"})
     * @Assert\NotBlank()
     */
    private $name;
    /**
     * @var int оценка ресторана
     * @ORM\Column(name="rating", type="integer", options={"comment":"Оценка ресторана"})
     */
    private $rating;
    /**
     * @var ArrayCollection Права доступа принадлежащие ролям
     * @ORM\ManyToMany(targetEntity="App\Entity\Adres", inversedBy="restoran")
     * @ORM\JoinTable(name="restorans_adress",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_restoran", referencedColumnName="id_restoran")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_adres", referencedColumnName="id_adres")
     *  }
     * )
     */
    private $adress;
    /**
     * @var string URL изображения
     * @ORM\Column(name="imageLink", type="string", length=255, nullable=true, options={"comment":"Ссылка на изображение профиля"})
     */
    private $imageLink;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tovar", mappedBy="restoran")
     */
    private $tovars;

    public function __construct()
    {
        $this->adress = new ArrayCollection();
        $this->tovars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getImageLink(): ?string
    {
        return $this->imageLink;
    }

    public function setImageLink(?string $imageLink): self
    {
        $this->imageLink = $imageLink;

        return $this;
    }

    /**
     * @return Collection|Adres[]
     */
    public function getAdress(): Collection
    {
        return $this->adress;
    }

    public function addAdress(Adres $adress)
    {
        if (!$this->adress->contains($adress)) {
            $this->adress[] = $adress;
        }

        return $this;
    }

    public function removeAdress(Adres $adress)
    {
        if ($this->adress->contains($adress)) {
            $this->adress->removeElement($adress);
        }

        return $this;
    }

    /**
     * @return Collection|Tovar[]
     */
    public function getTovars(): Collection
    {
        return $this->tovars;
    }

    public function addTovar(Tovar $tovar): self
    {
        if (!$this->tovars->contains($tovar)) {
            $this->tovars[] = $tovar;
            $tovar->setRestoran($this);
        }

        return $this;
    }

    public function removeTovar(Tovar $tovar): self
    {
        if ($this->tovars->contains($tovar)) {
            $this->tovars->removeElement($tovar);
            // set the owning side to null (unless already changed)
            if ($tovar->getRestoran() === $this) {
                $tovar->setRestoran(null);
            }
        }

        return $this;
    }


}
