<?php
/**
 * Created by PhpStorm.
 * User: Sudoku Laboratory
 * Date: 06.07.2018
 * Time: 18:40
 */

namespace Admin\Controller;



use Admin\Form\Type\RestoranType;
use App\Entity\Adres;
use App\Entity\Restoran;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\Zakaz;
use App\Services\ImageUploader;
use App\Services\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

/**
 * Class ZakaziController
 * @package Admin\Controller
 * @Route("/admin/zakazi", name="admin_zakazi_")
 */
class ZakaziController extends AbstractController
{
    const MODULE_NAME = "ZAKAZ";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    const LOG_CREATE =  'Создание новой роли';
    const LOG_UPDATE =  'Изменение роли';
    const LOG_VIEW = 'Просмотр списка всех ролей';
    const LOG_VIEWONE = 'Просмотр роли';
    const LOG_DELETE =  'Удаление роли';

    const LOG_UPLOAD_URL = 'Загрузка фотографии профиля %url%';
    const LOG_DELETE_URL = 'Удаление фотографии профиля %url%';
    const LOG_ERROR_DELETED_URL = 'Ошибка удаления фотографии профиля %url%';


    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * Вывод списка заказов
     *
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $zakaz = $this->getDoctrine()->getRepository(Zakaz::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW, self::LOG_VIEW);

        return $this->render('admin/zakaz/zakaz.html.twig', array('zakaz' => $zakaz));
    }

    /**
     * Показать ресторан
     *
     * @Route("/{id}/show",name="show", requirements={"id"="\d+"})
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        /**
         * @var Zakaz $zakaz
         */
        $zakaz = $this->getDoctrine()->getRepository(Zakaz::class)->find($id);

        if (!$zakaz) {
            throw $this->createNotFoundException('Заказ не найден');
        }
        $this->logger->addLog(self::LOG_VIEWONE,self::LOG_VIEWONE);


        return $this->render('admin/zakaz/show.html.twig', array(
            'zakaz' => $zakaz,
        ));
    }

    /**
     * Вывод списка
     *
     * @Route("/get", name="json")
     *
     * @param Request $request
     * @return Response
     */
    public function getJson(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        if ($request->isXMLHttpRequest()) {
            $search = $request->get('search')['value'];
            $limit = $request->get('length');
            $offset = $request->get('start');

            $repository = $this->getDoctrine()->getRepository(Restoran::class);

            if (!$search)
                $users = $repository->findBy([], [], $limit, $offset);
            else
                $users = $repository->searchUsernameOrEmail($search, $offset, $limit);
            $this->logger->addLog(self::LOG_VIEW, '-');

            return new JsonResponse(
                array(
                    'draw' => $request->get('draw'),
                    'recordsTotal' => $repository->count([]),
                    'recordsFiltered' => $repository->count([]),
                    'data' => $users,
                )
            );
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * Добавление ресторана
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::CREATE);
        $adres = new Adres();
        $restoran = new Restoran();
        $form = $this->createForm(RestoranType::class,$restoran);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $city = $form->get('city')->getData();
            $street = $form->get('street')->getData();
            $house = $form->get('house')->getData();
            $porch = $form->get('porch')->getData();
            $apartment = $form->get('apartment')->getData();
            $adres->setCity($city);
            $adres->setStreet($street);
            $adres->setHouse($house);
            $adres->setPorch($porch);
            $adres->setApartment($apartment);
            $adress = $this->getDoctrine()->getManager();
            $adress->persist($adres);
            $adress->flush();
            $restoran->addAdress($adres);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restoran);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);
            return $this->redirectToRoute('admin_restoran_index');
        }

        return $this->render('admin/restoran/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Добавление ресторана'
        ));
    }

    /**
     * Редактирование ресторана
     *
     * @Route("/{id}/edit", name="edit", requirements={"id"="\d+"})
     *
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::UPDATE);
        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->find($id);

        $form = $this->createForm(RestoranType::class, $restoran);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $restoran = $form->getData();
            foreach ($form->get('adres')->getData() as $item){
                $restoran->addAdress($item);
                $restoran->getName();
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restoran);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_UPDATE,self::LOG_UPDATE);

            return $this->redirectToRoute('admin_restoran_index');
        }

        return $this->render('admin/restoran/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Редактирование ресторана'
        ));
    }

    /**
     * Удаление ресторана
     *
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $zakaz = $this->getDoctrine()->getRepository(Zakaz::class)->find($id);

        if (!$zakaz) {
            throw $this->createNotFoundException('Заказ не найден.');
        }
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($zakaz);
        $entityManager->flush();
        return $this->redirectToRoute('admin_zakazi_index');
    }
    /**
     * Удаление ресторана
     *
     * @Route("/{id}/success", name="success", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function success(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::UPDATE);

        $zakaz= $this->getDoctrine()->getRepository(Zakaz::class)->find($id);

        if (!$zakaz) {
            throw $this->createNotFoundException('Заказ не найден.');
        }
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE);
        $status = $this->getDoctrine()->getManager()->getRepository(Status::class)->find($zakaz->getStatus()->getId()+1);
        $zakaz->setStatus($status);
        $zakazz = $this->getDoctrine()->getManager();
        $zakazz->persist($zakaz);
        $zakazz->flush();
        return $this->redirectToRoute('admin_zakazi_show', array('id'=>$id));
    }

}