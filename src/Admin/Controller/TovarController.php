<?php
/**
 * Created by PhpStorm.
 * User: Sudoku Laboratory
 * Date: 06.07.2018
 * Time: 18:40
 */

namespace Admin\Controller;


use Admin\Form\Type\CategoryCharacteristicType;
use Admin\Form\Type\CategotyType;
use Admin\Form\Type\CharacteristicType;
use Admin\Form\Type\TipTovarType;
use Admin\Form\Type\TovarCharacteristicType;
use Admin\Form\Type\TovarType;
use App\Entity\Category;
use App\Entity\CategoryCharacteristic;
use App\Entity\Characteristic;
use App\Entity\Restoran;
use App\Entity\Tip;
use App\Entity\Tovar;
use App\Entity\TovarCharacteristic;
use App\Entity\User;
use App\Services\ImageUploader;
use App\Services\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

/**
 * Class TovarController
 * @package Admin\Controller
 * @Route("/admin/tovar", name="admin_tovar_")
 */
class TovarController extends AbstractController
{
    const MODULE_NAME = "TOVAR";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    const LOG_CREATE =  'Создание новой роли';
    const LOG_UPDATE =  'Изменение роли';
    const LOG_VIEW = 'Просмотр списка всех ролей';
    const LOG_VIEWONE = 'Просмотр роли';
    const LOG_DELETE =  'Удаление роли';
    const LOG_UPLOAD_URL = 'Загрузка фотографии профиля %url%';
    const LOG_DELETE_URL = 'Удаление фотографии профиля %url%';
    const LOG_ERROR_DELETED_URL = 'Ошибка удаления фотографии профиля %url%';

    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * Вывод списка товаров
     *
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $tovar = $this->getDoctrine()->getRepository(Tovar::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW, self::LOG_VIEW);

        return $this->render('admin/tovar/tovar.html.twig', array('tovars' => $tovar));
    }

    /**
     * Показать товар
     *
     * @Route("/{id}/show",name="show", requirements={"id"="\d+"})
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        /**
         * @var Restoran $restoran
         */
        $tovar = $this->getDoctrine()->getRepository(Tovar::class)->find($id);

        if (!$tovar) {
            throw $this->createNotFoundException('Данный продукт не найден');
        }

        $session_us = $this->getUser()->getId();
        $userr = $this->getDoctrine()->getManager()->getRepository(User::class)->find($session_us);

        $this->logger->addLog(self::LOG_VIEWONE,self::LOG_VIEWONE);

        if ($tovar->getImageLink()) {
            $image = new File($this->getParameter('imgDir') . '/' . $tovar->getImageLink());
            $imageName = $tovar->getImageLink();
            $size = $image->getSize();
            $type = $image->getType();

            return $this->render('admin/tovar/show.html.twig', array(
                'tovar' => $tovar,
                'session_u' => $session_us,
                'image' => $imageName,
                'size' => $size,
                'type' => $type,
                'users' => $userr,
            ));
        }
        return $this->render('admin/tovar/show.html.twig', array(
            'tovar' => $tovar,
            'session_u' => $session_us,
            'users' => $userr,
            ));

    }

    /**
     * Добавление товара
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
//        $this->denyAccessUnlessGranted(self::CREATE);
        $tovar = new Tovar();

        $categoryCharacteristic = new CategoryCharacteristic();
        $tovarCharacteristic = new TovarCharacteristic();

        $form = $this->createForm(TovarType::class,$tovar);

        $form3 =$this->createForm(CategoryCharacteristicType::class,$categoryCharacteristic);
        $form4 = $this->createForm(TovarCharacteristicType::class,$tovarCharacteristic);


        $form->handleRequest($request);

        $form3->handleRequest($request);
        $form4->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tovar = $form->getData();
            foreach ($form->get('category')->getData() as $item){
                $tovar->setIdCategory($item);
            }
            foreach ($form->get('tip')->getData() as $item){
                $tovar->addTip($item);
            }
            foreach ($form->get('restoran')->getData() as $item){
                $tovar->setRestoran($item);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tovar);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_tovar_index');
        }

        if ($form3->isSubmitted() && $form3->isValid()) {
            $categoryCharacteristic = $form->getData();
            foreach ($form3->get('category')->getData() as $item){
                $categoryCharacteristic->setIdCategory($item);
            }
            foreach ($form3->get('characteristic')->getData() as $item){
                $categoryCharacteristic->setIdCharacteristic($item);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categoryCharacteristic);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_tovar_index');
        }
        if ($form4->isSubmitted() && $form4->isValid()) {
            $tovarCharacteristic = $form->getData();
            foreach ($form4->get('CategoryCharacteristic')->getData() as $item){
                $tovarCharacteristic->setIdCategorycharacteristic($item);
            }
            foreach ($form4->get('tovar')->getData() as $item){
                $tovarCharacteristic->setIdTovar($item);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tovarCharacteristic);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_tovar_index');
        }
        return $this->render('admin/tovar/add.html.twig', array(
            'form' => $form->createView(),

            'form3' => $form3->createView(),
            'form4' => $form4->createView(),

            'title' => 'Добавление продукта',

            'title3' => 'Добавление категория-характеристики',
            'title4' => 'Добавление товар-характеристики',
        ));
    }

    /**
     * Редактирование роли
     *
     * @Route("/{id}/edit", name="edit", requirements={"id"="\d+"})
     *
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::UPDATE);
        $tovar = $this->getDoctrine()->getRepository(Tovar::class)->find($id);

        $categoryCharacteristic = $this->getDoctrine()->getRepository(CategoryCharacteristic::class)->find($id);
        $tovarCharacteristic = $this->getDoctrine()->getRepository(TovarCharacteristic::class)->find($id);


        $form = $this->createForm(TovarType::class, $tovar);

        $form3 = $this->createForm(CategoryCharacteristicType::class,$categoryCharacteristic);
        $form4 = $this->createForm(TovarCharacteristicType::class,$tovarCharacteristic);


        $form->handleRequest($request);

        $form3->handleRequest($request);
        $form4->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($form->get('category')->getData() as $item){
                $tovar->setIdCategory($item);
            }
            foreach ($form->get('tip')->getData() as $item){
                $tovar->addTip($item);
            }
            foreach ($form->get('restoran')->getData() as $item){
                $tovar->setestoran($item);
                $tovar->getName();
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tovar);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_tovar_index');
        }

        if ($form3->isSubmitted() && $form3->isValid()) {
            foreach ($form3->get('category')->getData() as $item){
                $categoryCharacteristic->setIdCategory($item);
            }

            foreach ($form3->get('characteristic')->getData() as $item){
                $categoryCharacteristic->setIdCharacteristic($item);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categoryCharacteristic);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_tovar_index');
        }
        if ($form4->isSubmitted() && $form4->isValid()) {
            foreach ($form4->get('CategoryCharacteristic')->getData() as $item){
                $tovarCharacteristic->setIdCategorycharacteristic($item);
            }
            foreach ($form4->get('tovar')->getData() as $item){
                $tovarCharacteristic->setIdTovar($item);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tovarCharacteristic);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_tovar_index');
        }


        return $this->render('admin/tovar/add.html.twig', array(
            'form' => $form->createView(),

            'form3' => $form3->createView(),
            'form4' => $form4->createView(),

            'title' => 'Добавление продукта',

            'title3' => 'Добавление категория-характеристики',
            'title4' => 'Добавление товар-характеристики'
        ));
    }

    /**
     * Удаление роли
     *
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $tovar = $this->getDoctrine()->getRepository(Tovar::class)->find($id);

        if (!$tovar) {
            throw $this->createNotFoundException('Данная роль не найдена.');
        }
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tovar);
        $entityManager->flush();
        return $this->redirectToRoute('admin_tovar_index');
    }

    /**
     * @Route("/{id}/upload", requirements={"id"="\d+"}, name="upload")
     * @param Request $request
     * @param ImageUploader $imageUploader
     * @param $id
     * @return JsonResponse
     */
    public function uploadAction(Request $request, ImageUploader $imageUploader,$id)
    {
        $this->denyAccessUnlessGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY);

        $output = [
            'uploaded' => false
        ];

        $uploadFile = $request->files->get('file');

        /** @var Tovar $tovar */
        $tovar = $this->getDoctrine()->getRepository(Tovar::class)->find($id);

        if ($uploadFile instanceof UploadedFile) {
            $fileName = $imageUploader->upload($uploadFile);

            if ($fileName != null) {

                $tovar->setImageLink($fileName);

                $em = $this->getDoctrine()->getManager();
                $em->persist($tovar);
                $em->flush();
                $this->logger->addLog(self::LOG_UPLOAD_URL, 'Ресторан: '.$tovar->getName().' id: '. $tovar->getId());


                $output['uploaded'] = true;
                $output['filename'] = $fileName;
            }
        }

        return new JsonResponse($output);
    }


    /**
     * @Route("/{id}/deletePhoto",  requirements={"id"="\d+"},name="deletePhoto")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function deletePhotoAction(Request $request,$id)
    {
        $this->denyAccessUnlessGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY);

        /** @var Tovar $tovar */
        $tovar = $this->getDoctrine()->getRepository(Tovar::class)->find($id);

        if ($tovar->getImageLink() != null) {
            try {
                $fs = new Filesystem();
                $fs->remove($this->getParameter('imgDir') . '/' . $tovar->getImageLink());
                $this->logger->addLog(self::LOG_DELETE_URL, 'Логин: '.$tovar->getName().' id: '. $tovar->getId());

            } catch (IOExceptionInterface  $e) {
                $this->logger->addLog(self::LOG_ERROR_DELETED_URL, 'Логин: '.$tovar->getName().' id: '. $tovar->getId());
                echo 'error';
            }
            $tovar->setImageLink(null);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tovar);
            $entityManager->flush();
        } else {
            throw $this->createNotFoundException('The image does not exist');
        }

        return new JsonResponse(['Status' => "Success"]);
    }

}