<?php
/**
 * Created by PhpStorm.
 * User: Sudoku Laboratory
 * Date: 06.07.2018
 * Time: 18:40
 */

namespace Admin\Controller;



use Admin\Form\Type\CategotyType;
use Admin\Form\Type\CharacteristicType;
use Admin\Form\Type\RestoranType;
use Admin\Form\Type\TipTovarType;
use App\Entity\Adres;
use App\Entity\Category;
use App\Entity\Characteristic;
use App\Entity\Restoran;
use App\Entity\Tip;
use App\Entity\User;
use App\Services\ImageUploader;
use App\Services\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

/**
 * Class RestoranController
 * @package Admin\Controller
 * @Route("/admin/xarakteristika", name="admin_xarakteristika_")
 */
class XarakteristikaController extends AbstractController
{
    const MODULE_NAME = "TIP";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    const LOG_CREATE =  'Создание новой роли';
    const LOG_UPDATE =  'Изменение роли';
    const LOG_VIEW = 'Просмотр списка всех ролей';
    const LOG_VIEWONE = 'Просмотр роли';
    const LOG_DELETE =  'Удаление роли';

    const LOG_UPLOAD_URL = 'Загрузка фотографии профиля %url%';
    const LOG_DELETE_URL = 'Удаление фотографии профиля %url%';
    const LOG_ERROR_DELETED_URL = 'Ошибка удаления фотографии профиля %url%';


    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * Вывод списка всех категории
     *
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $xar = $this->getDoctrine()->getRepository(Characteristic::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW, self::LOG_VIEW);

        return $this->render('admin/characteristica/xarakteristica.html.twig', array('xar' => $xar));
    }

    /**
     * Показать категорию
     *
     * @Route("/{id}/show",name="show", requirements={"id"="\d+"})
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        /**
         * @var Tip $tip
         */
        $xar = $this->getDoctrine()->getRepository(Characteristic::class)->find($id);

        if (!$xar) {
            throw $this->createNotFoundException('Характеристика не найден');
        }
        $this->logger->addLog(self::LOG_VIEWONE,self::LOG_VIEWONE);

        $session_us = $this->getUser()->getId();
        $userr = $this->getDoctrine()->getManager()->getRepository(User::class)->find($session_us);

        return $this->render('admin/characteristica/show.html.twig', array(
            'xar' => $xar,
            'session_u' => $session_us,
            'users' => $userr,
            ));
    }

    /**
     * Вывод списка
     *
     * @Route("/get", name="json")
     *
     * @param Request $request
     * @return Response
     */
    public function getJson(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        if ($request->isXMLHttpRequest()) {
            $search = $request->get('search')['value'];
            $limit = $request->get('length');
            $offset = $request->get('start');

            $repository = $this->getDoctrine()->getRepository(Characteristic::class);

            if (!$search)
                $users = $repository->findBy([], [], $limit, $offset);
            else
                $users = $repository->searchUsernameOrEmail($search, $offset, $limit);
            $this->logger->addLog(self::LOG_VIEW, '-');

            return new JsonResponse(
                array(
                    'draw' => $request->get('draw'),
                    'recordsTotal' => $repository->count([]),
                    'recordsFiltered' => $repository->count([]),
                    'data' => $users,
                )
            );
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * Добавление категории
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::CREATE);
        $characteristic = new Characteristic();
        $form = $this->createForm(CharacteristicType::class,$characteristic);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $characteristic = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($characteristic);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_xarakteristika_index');
        }
        return $this->render('admin/characteristica/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Добавление характеристики'
        ));
    }

    /**
     * Редактирование категории
     *
     * @Route("/{id}/edit", name="edit", requirements={"id"="\d+"})
     *
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::UPDATE);
        $characteristic = $this->getDoctrine()->getRepository(Characteristic::class)->find($id);

        $form = $this->createForm(CharacteristicType::class,$characteristic);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $characteristic = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($characteristic);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_UPDATE,self::LOG_UPDATE);

            return $this->redirectToRoute('admin_xarakteristika_index');
        }

        return $this->render('admin/characteristica/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Редактирование характеристики'
        ));
    }

    /**
     * Удаление ресторана
     *
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $xar = $this->getDoctrine()->getRepository(Characteristic::class)->find($id);

        if (!$xar) {
            throw $this->createNotFoundException('Характеристика не найдена.');
        }
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($xar);
        $entityManager->flush();
        return $this->redirectToRoute('admin_xarakteristika_index');
    }
}