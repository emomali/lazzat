<?php
/**
 * Created by PhpStorm.
 * User: Sudoku Laboratory
 * Date: 06.07.2018
 * Time: 18:40
 */

namespace Admin\Controller;



use Admin\Form\Type\RestoranType;
use App\Entity\Adres;
use App\Entity\Restoran;
use App\Entity\User;
use App\Services\ImageUploader;
use App\Services\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

/**
 * Class RestoranController
 * @package Admin\Controller
 * @Route("/admin/restoran", name="admin_restoran_")
 */
class RestoranController extends AbstractController
{
    const MODULE_NAME = "RESTORAN";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    const LOG_CREATE =  'Создание новой роли';
    const LOG_UPDATE =  'Изменение роли';
    const LOG_VIEW = 'Просмотр списка всех ролей';
    const LOG_VIEWONE = 'Просмотр роли';
    const LOG_DELETE =  'Удаление роли';

    const LOG_UPLOAD_URL = 'Загрузка фотографии профиля %url%';
    const LOG_DELETE_URL = 'Удаление фотографии профиля %url%';
    const LOG_ERROR_DELETED_URL = 'Ошибка удаления фотографии профиля %url%';


    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * Вывод списка ролей
     *
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW, self::LOG_VIEW);

        return $this->render('admin/restoran/restoran.html.twig', array('restorans' => $restoran));
    }

    /**
     * Показать ресторан
     *
     * @Route("/{id}/show",name="show", requirements={"id"="\d+"})
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        /**
         * @var Restoran $restoran
         */
        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->find($id);

        if (!$restoran) {
            throw $this->createNotFoundException('Данный ресторан не найден');
        }
        $this->logger->addLog(self::LOG_VIEWONE,self::LOG_VIEWONE);

        $session_us = $this->getUser()->getId();
        $userr = $this->getDoctrine()->getManager()->getRepository(User::class)->find($session_us);

        if ($restoran->getImageLink()) {
            $image = new File($this->getParameter('imgDir') . '/' . $restoran->getImageLink());
            $imageName = $restoran->getImageLink();
            $size = $image->getSize();
            $type = $image->getType();
            dump($session_us);

            return $this->render('admin/restoran/show.html.twig', array(
                'restorans' => $restoran,
                'session_u' => $session_us,
                'image' => $imageName,
                'size' => $size,
                'type' => $type,
                'users' => $userr,
            ));
        }

        return $this->render('admin/restoran/show.html.twig', array(
            'restorans' => $restoran,
            'session_u' => $session_us,
            'users' => $userr,
            ));
    }

    /**
     * Вывод списка
     *
     * @Route("/get", name="json")
     *
     * @param Request $request
     * @return Response
     */
    public function getJson(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        if ($request->isXMLHttpRequest()) {
            $search = $request->get('search')['value'];
            $limit = $request->get('length');
            $offset = $request->get('start');

            $repository = $this->getDoctrine()->getRepository(Restoran::class);

            if (!$search)
                $users = $repository->findBy([], [], $limit, $offset);
            else
                $users = $repository->searchUsernameOrEmail($search, $offset, $limit);
            $this->logger->addLog(self::LOG_VIEW, '-');

            return new JsonResponse(
                array(
                    'draw' => $request->get('draw'),
                    'recordsTotal' => $repository->count([]),
                    'recordsFiltered' => $repository->count([]),
                    'data' => $users,
                )
            );
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * Добавление ресторана
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::CREATE);
        $adres = new Adres();
        $restoran = new Restoran();
        $form = $this->createForm(RestoranType::class,$restoran);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $city = $form->get('city')->getData();
            $street = $form->get('street')->getData();
            $house = $form->get('house')->getData();
            $porch = $form->get('porch')->getData();
            $apartment = $form->get('apartment')->getData();
            $adres->setCity($city);
            $adres->setStreet($street);
            $adres->setHouse($house);
            $adres->setPorch($porch);
            $adres->setApartment($apartment);
            $adress = $this->getDoctrine()->getManager();
            $adress->persist($adres);
            $adress->flush();
            $restoran->addAdress($adres);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restoran);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);
            return $this->redirectToRoute('admin_restoran_index');
        }

        return $this->render('admin/restoran/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Добавление ресторана'
        ));
    }

    /**
     * Редактирование ресторана
     *
     * @Route("/{id}/edit", name="edit", requirements={"id"="\d+"})
     *
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::UPDATE);
        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->find($id);

        $form = $this->createForm(RestoranType::class, $restoran);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $restoran = $form->getData();
            foreach ($form->get('adres')->getData() as $item){
                $restoran->addAdress($item);
                $restoran->getName();
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restoran);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_UPDATE,self::LOG_UPDATE);

            return $this->redirectToRoute('admin_restoran_index');
        }

        return $this->render('admin/restoran/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Редактирование ресторана'
        ));
    }

    /**
     * Удаление ресторана
     *
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->find($id);

        if (!$restoran) {
            throw $this->createNotFoundException('Данная роль не найдена.');
        }
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($restoran);
        $entityManager->flush();
        return $this->redirectToRoute('admin_restoran_index');
    }
    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * @Route("/{id}/upload", requirements={"id"="\d+"}, name="upload")
     * @param Request $request
     * @param ImageUploader $imageUploader
     * @param $id
     * @return JsonResponse
     */
    public function uploadAction(Request $request, ImageUploader $imageUploader,$id)
    {
        $this->denyAccessUnlessGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY);

        $output = [
            'uploaded' => false
        ];

        $uploadFile = $request->files->get('file');

        /** @var Restoran $restoran */
        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->find($id);

        if ($uploadFile instanceof UploadedFile) {
            $fileName = $imageUploader->upload($uploadFile);

            if ($fileName != null) {

                $restoran->setImageLink($fileName);

                $em = $this->getDoctrine()->getManager();
                $em->persist($restoran);
                $em->flush();
                $this->logger->addLog(self::LOG_UPLOAD_URL, 'Ресторан: '.$restoran->getName().' id: '. $restoran->getId());


                $output['uploaded'] = true;
                $output['filename'] = $fileName;
            }
        }

        return new JsonResponse($output);
    }


    /**
     * @Route("/{id}/deletePhoto",  requirements={"id"="\d+"},name="deletePhoto")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function deletePhotoAction(Request $request,$id)
    {
        $this->denyAccessUnlessGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY);

        /** @var Restoran $restoran */
        $restoran = $this->getDoctrine()->getRepository(Restoran::class)->find($id);

        if ($restoran->getImageLink() != null) {
            try {
                $fs = new Filesystem();
                $fs->remove($this->getParameter('imgDir') . '/' . $restoran->getImageLink());
                $this->logger->addLog(self::LOG_DELETE_URL, 'Логин: '.$restoran->getName().' id: '. $restoran->getId());

            } catch (IOExceptionInterface  $e) {
                $this->logger->addLog(self::LOG_ERROR_DELETED_URL, 'Логин: '.$restoran->getName().' id: '. $restoran->getId());
                echo 'error';
            }
            $restoran->setImageLink(null);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restoran);
            $entityManager->flush();
        } else {
            throw $this->createNotFoundException('The image does not exist');
        }

        return new JsonResponse(['Status' => "Success"]);
    }



}