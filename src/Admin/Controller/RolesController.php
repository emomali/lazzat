<?php
/**
 * Created by PhpStorm.
 * User: Sudoku Laboratory
 * Date: 06.07.2018
 * Time: 18:40
 */

namespace Admin\Controller;



use App\Entity\Role;
use App\Form\RoleType;
use App\Services\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RolesController
 * @package Admin\Controller
 * @Route("/admin/roles", name="admin_roles_")
 */
class RolesController extends AbstractController
{
    const MODULE_NAME = "ROLES";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    const LOG_CREATE =  'Создание новой роли';
    const LOG_UPDATE =  'Изменение роли';
    const LOG_VIEW = 'Просмотр списка всех ролей';
    const LOG_VIEWONE = 'Просмотр роли';
    const LOG_DELETE =  'Удаление роли';

    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * Вывод списка ролей
     *
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $roles = $this->getDoctrine()->getRepository(Role::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW, self::LOG_VIEW);

        return $this->render('admin/roles/role.html.twig', array('roles' => $roles));
    }

    /**
     * Показать роль
     *
     * @Route("/{id}/show",name="show", requirements={"id"="\d+"})
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        /**
         * @var Role $role
         */
        $role = $this->getDoctrine()->getRepository(Role::class)->find($id);
        if (!$role) {
            throw $this->createNotFoundException('Данная роль не найдена');
        }
        $this->logger->addLog(self::LOG_VIEWONE,self::LOG_VIEWONE);

        return $this->render('admin/roles/show.html.twig', array('role' => $role));

    }

    /**
     * Добавление роли
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::CREATE);
        $role = new Role();
        $form = $this->createForm(RoleType::class, $role);
        dump($role);
        dump($form);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $role = $form->getData();
            foreach ($form->get('permissions')->getData() as $item){
                $role->addPermission($item);
            }


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($role);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_roles_index');
        }

        return $this->render('admin/roles/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Добавление роли'
        ));
    }

    /**
     * Редактирование роли
     *
     * @Route("/{id}/edit", name="edit", requirements={"id"="\d+"})
     *
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::UPDATE);
        $role = $this->getDoctrine()->getRepository(Role::class)->find($id);

        $form = $this->createForm(RoleType::class, $role);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $role = $form->getData();
            foreach ($form->get('permissions')->getData() as $item){
                $role->addPermission($item);
                $role->getName();
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($role);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_UPDATE,self::LOG_UPDATE);

            return $this->redirectToRoute('admin_roles_index');
        }

        return $this->render('admin/roles/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Редактирование роли'
        ));
    }

    /**
     * Удаление роли
     *
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $role = $this->getDoctrine()->getRepository(Role::class)->find($id);

        if (!$role) {
            throw $this->createNotFoundException('Данная роль не найдена.');
        }
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($role);
        $entityManager->flush();
        return $this->redirectToRoute('admin_roles_index');
    }
}