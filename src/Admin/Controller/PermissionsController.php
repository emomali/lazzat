<?php
/**
 * Created by PhpStorm.
 * User: Sudoku Laboratory
 * Date: 07.07.2018
 * Time: 17:31
 */

namespace Admin\Controller;

use App\Entity\Permission;
use App\Form\PermissionType;
use App\Services\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PermissionsController
 * @package Admin\Controller
 * @Route("/admin/permissions", name="admin_permissions_")
 */
class PermissionsController extends AbstractController
{
    const MODULE_NAME="PERMISSIONS";

    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE= self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    const LOG_CREATE =  'Создание нового права доступа';
    const LOG_UPDATE =  'Изменение права доступа';
    const LOG_VIEW =  'Просмотр списка всех прав доступа';
    const LOG_VIEWONE =  'Просмотр права доступа';
    const LOG_DELETE=  'Удаление права доступа';


    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }
    /**
     * Вывод списка разрешений
     *
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction() {
        $this->denyAccessUnlessGranted(self::VIEW);

        $permissions = $this->getDoctrine()->getManager()->getRepository(Permission::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW,self::LOG_VIEW);

        return $this->render('admin/permissions/permission.html.twig', array(
            'permissions' => $permissions,
        ));
    }


    /**
     * Просмотр  разрешения
     *
     * @Route("/{id}/show", name="show", requirements={"id"="\d+"})
     * @param $id;
     * @return Response
     */
    public function showAction($id) {
        $this->denyAccessUnlessGranted(self::VIEW);

        $permission = $this->getDoctrine()->getRepository(Permission::class)->find($id);
        $this->logger->addLog(self::LOG_VIEWONE, self::LOG_VIEWONE . ' Логин: '.$permission->getName() .' id: '. $permission->getId());

        return $this->render('admin/permissions/show.html.twig', array(
            'permission' => $permission,
        ));
    }


    /**
     * Добавление разрежения
     * @Route("/add", name = "add")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     */
    public function createAction(Request $request){
        $this->denyAccessUnlessGranted(self::CREATE);

        $pesmission = new Permission();

        $form = $this-> createForm(PermissionType::class,$pesmission);

        $form-> handleRequest($request);

        if($form->isSubmitted()&& $form->isValid()){
            $permission = $form->getData();

            foreach ($form->get('roles')->getData() as $item){
                $permission->addRole($item);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($permission);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE, self::LOG_CREATE);

            return $this->redirectToRoute('admin_permissions_index');
        }
        return $this->render('admin/permissions/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Добавление права доступа'
        ));
    }




    /**
     * Добавление разрежения
     * @Route("/{id}/edit", name = "edit")
     *
     * @param $id
     *
     * @return RedirectResponse|Response
     *
     */
    public function editAction(Request $request,$id){
        $this->denyAccessUnlessGranted(self::UPDATE);

        $pesmission = $this->getDoctrine()->getRepository(Permission::class)->find($id);

        $form = $this-> createForm(PermissionType::class,$pesmission);

        $form-> handleRequest($request);

        if($form->isSubmitted()&& $form->isValid()){
            $permission = $form->getData();
            foreach ($form->get('roles')->getData() as $item){
                $permission->addRole($item);
                $permission->getName();
            }


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($permission);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_UPDATE, self::LOG_UPDATE . ' Логин: '.$permission->getName() .' id: '. $permission->getId());

            return $this->redirectToRoute('admin_permissions_index');
        }
        return $this->render('admin/permissions/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Редактирование разрешения'
        ));
    }

    /**
     * Удаление разрешения
     *
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function deleteAction(Request $request, $id) {

        $this->denyAccessUnlessGranted(self::DELETE);

        $permission = $this->getDoctrine()->getRepository(Permission::class)->find($id);

        if(!$permission) {
            throw $this->createNotFoundException('Данное разрешение не найдено');
        }
        $this->logger->addLog(self::LOG_DELETE,self::LOG_DELETE . ' Логин: '.$permission->getName() .' id: '. $permission->getId());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($permission);
        $entityManager->flush();
        return $this->redirectToRoute('admin_permissions_index');
    }
}
