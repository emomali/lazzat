<?php


namespace Admin\Controller;


use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Form\PermissionType;
use App\Form\RoleType;
use App\Form\UserType;
use App\Controller\RegisterController;
use App\Services\Logger;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersController extends AbstractController
{
    const MODULE_NAME = "USERS";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const DELETE = self::MODULE_NAME . '_DELETE';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const EDIT = self::MODULE_NAME . "_EDIT";

    const LOG_CREATE = 'Создание пользователья';
    const LOG_UPDATE = 'Изменение пользователья';
    const LOG_VIEW = 'Просмотр всех пользовательей';
    const LOG_VIEWONE = 'Просмотр пользователья';
    const LOG_DELETE = 'Удаление пользователья';

    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("admin/users", name="admin_users")
     * @return Response
     */
    public function us(Request $request, Security $security)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $entityManager = $this->getDoctrine()->getManager();
        $userr = $entityManager->getRepository(User::class)->findAll();
        $roles = $this->getDoctrine()->getManager()->getRepository(Role::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW, self::LOG_VIEW);


        return $this->render('admin/users/users.html.twig', [
            'users' => $userr,
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("admin/users/{id}/show",name="show", requirements={"id"="\d+"})
     *
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $this->logger->addLog(self::LOG_VIEWONE, self::LOG_VIEWONE . ' Логин: ' . $user->getUsername() . ' id: ' . $user->getId());

        $session_us = $this->getUser()->getId();

        return $this->render('admin/users/show.html.twig', array(
            'user' => $user,
            'session_u' => $session_us,
        ));
    }

    /**
     * Редактирование пользователя
     *
     * @Route("admin/users/{id}/edit", name="edit", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, $id)
    {

        $this->denyAccessUnlessGranted(self::EDIT);
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $this->logger->addLog(self::LOG_UPDATE, self::LOG_UPDATE . ' Логин: ' . $user->getUsername() . ' id: ' . $user->getId());


        if (!$user) {
            throw $this->createNotFoundException('The user does not exist');
        }

        $originSalt = $user->getSalt();
        $originPassword = $user->getPassword();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            // Проверяем изменялся ли пароль
            if ($user->getPassword()) {
                // Меняем соль и генерируем пароль
                $encoder = new \Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder(12);
                $user->setSalt(time());
                $user->setPassword($encoder->encodePassword($user->getUsername(), $user->getSalt()));
            } else {
                // Возвращаем старое значение пароля
                $user->setSalt($originSalt);
                $user->setPassword($originPassword);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_UPDATE, 'Логин: ' . $user->getUsername() . ' id: ' . $user->getId());

            // TODO сделать логгирование

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('security/register.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Изменить пользователя',
        ));
    }

    /**
     * Удаление пользователей
     *
     * @Route("admin/users/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {

        $this->denyAccessUnlessGranted(self::DELETE);

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE . ' Логин: ' . $user->getUsername() . ' id: ' . $user->getId());

        if (!$user) {
            throw $this->createNotFoundException('The user does not exist');
        }

        // TODO Сделать отправку на e-mail и логгирование
        $this->logger->addLog(self::LOG_DELETE, 'Логин: ' . $user->getUsername() . ' id: ' . $user->getId());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('admin_users');
    }


}