<?php


namespace Admin\Controller;


use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Form\PermissionType;
use App\Form\RoleType;
use App\Form\UserType;
use App\Controller\RegisterController;
use App\Repository\ForStatisticRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
/**
 * Class AdminController
 * @package Admin\Controller
 * @Route("/admin", name="admin_")
 */

class AdminController extends AbstractController
{
    const MODULE_NAME="ADMIN";

    const VIEW = self::MODULE_NAME . '_VIEW';
    /**
     * @Route("/", name="home_index")
     * @return Response
     */
    public function us(Request $request,Security $security)
    {
//        $this->denyAccessUnlessGranted(self::VIEW);

        $entityManager = $this->getDoctrine()->getManager();
        $userr = $entityManager->getRepository(User::class)->findAll();
        $roles = $this->getDoctrine()->getManager()->getRepository(Role::class)->findAll();
        $user = new User();
        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'isAdmin'=> $this->isGranted("ROLE_ADMIN") ? true : false
            ]
        );


        $session_us = $this->getUser()->getId();


        $encoder = new \Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder(12);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            dump($user);
            $user->setSalt(time());
            $user->setPassword($encoder->encodePassword($user->getUsername(), $user->getSalt()));

            if ($this->isGranted("ROLE_ADMIN")) {
                foreach ($form->get('userRoles')->getData() as $item) {
                    $user->addUserRole($item);
                }
            } else {
                $role = $this->getDoctrine()->getRepository(Role::class)->findOneBy(array('name' => "ROLE_USER"));
                $user->addUserRole($role);
            }


            //$user->setPassword($password);

            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();
        }
        $usr = $security->getUser();
        $usr->getUsername();
//        $statistic_beg= $this->getDoctrine()->getRepository(User::class)->getforStatistic();
        return $this->render('admin/index.html.twig', [
            'users' => $userr,
            'roles' => $roles,
            'form' => $form->createView(),
            'usr' => $usr,
            'session_u' => $session_us,
        ]);
    }
}