<?php


namespace Admin\Controller;


use Admin\Form\Type\CourierType;
use App\Entity\Courier;
use App\Entity\Role;
use App\Services\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
/**
 * Class CourierController
 * @package Admin\Controller
 * @Route("/admin/courier", name="admin_courier_")
 */

class CourierController extends AbstractController
{
    const MODULE_NAME = "COURIER";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const DELETE = self::MODULE_NAME . '_DELETE';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const EDIT = self::MODULE_NAME . "_EDIT";

    const LOG_CREATE = 'Создание пользователья';
    const LOG_UPDATE = 'Изменение пользователья';
    const LOG_VIEW = 'Просмотр всех пользовательей';
    const LOG_VIEWONE = 'Просмотр пользователья';
    const LOG_DELETE = 'Удаление пользователья';

    protected $logger;

    /**
     * PlannedController constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/", name="index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function us(Request $request, Security $security)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        $entityManager = $this->getDoctrine()->getManager();
        $courier = $entityManager->getRepository(Courier::class)->findAll();
//        $roles = $this->getDoctrine()->getManager()->getRepository(Role::class)->findAll();
        $this->logger->addLog(self::LOG_VIEW, self::LOG_VIEW);


        return $this->render('admin/courier/courier.html.twig', [
            'couriers' => $courier
//            'roles' => $roles,
        ]);
    }
    /**
     * Показать роль
     *
     * @Route("/{id}/show",name="show", requirements={"id"="\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);
        /**
         * @var Role $role
         */
        $courier = $this->getDoctrine()->getRepository(Courier::class)->find($id);
        if (!$courier) {
            throw $this->createNotFoundException('Данный курьер не найден');
        }
        $this->logger->addLog(self::LOG_VIEWONE,self::LOG_VIEWONE);

        return $this->render('admin/courier/show.html.twig', array('courier' => $courier));

    }

    /**
     * Добавление роли
     *
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::CREATE);
        $courier = new Courier();
        $form = $this->createForm(CourierType::class, $courier);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $courier = $form->getData();
            $role = $this->getDoctrine()->getRepository(Role::class)->findOneBy(array('name' => "ROLE_COURIER"));
            $courier->setRole($role);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($courier);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_CREATE,self::LOG_CREATE);

            return $this->redirectToRoute('admin_courier_index');
        }

        return $this->render('admin/courier/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Добавление курьера'
        ));
    }

    /**
     * Редактирование роли
     *
     * @Route("/{id}/edit", name="edit", requirements={"id"="\d+"})
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::EDIT);
        $courier = $this->getDoctrine()->getRepository(Courier::class)->find($id);

        $form = $this->createForm(CourierType::class, $courier);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $courier = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($courier);
            $entityManager->flush();
            $this->logger->addLog(self::LOG_UPDATE,self::LOG_UPDATE);

            return $this->redirectToRoute('admin_courier_index');
        }

        return $this->render('admin/courier/add.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Редактирование курьера'
        ));
    }

    /**
     * Удаление роли
     *
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $courier = $this->getDoctrine()->getRepository(Courier::class)->find($id);

        if (!$courier) {
            throw $this->createNotFoundException('Данный курьер не найден.');
        }
        $this->logger->addLog(self::LOG_DELETE, self::LOG_DELETE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($courier);
        $entityManager->flush();
        return $this->redirectToRoute('admin_courier_index');
    }

}