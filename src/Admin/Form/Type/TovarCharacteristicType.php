<?php


namespace Admin\Form\Type;

use App\Entity\Category;
use App\Entity\CategoryCharacteristic;
use App\Entity\Characteristic;
use App\Entity\Tovar;
use App\Entity\TovarCharacteristic;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TovarCharacteristicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tovar', EntityType::class, array(
                    'class' => Tovar::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'form-control',
                        'style' => 'margin:5px 0;'),

                )
            )
            ->add('CategoryCharacteristic', EntityType::class, array(
                    'class' => CategoryCharacteristic::class,
                    'choice_label' => 'id',
                    'multiple' => true,
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'form-control',
                        'style' => 'margin:5px 0;'),

                )
            )
            ->add('value',TextType::class,[
                'label' =>'value'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TovarCharacteristic::class
        ]);
    }



}