<?php


namespace Admin\Form\Type;


use App\Entity\Adres;
use App\Entity\Restoran;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormTypeInterface;

class RestoranType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                'label' =>'Название ресторана'
            ])
            ->add('rating',IntegerType::class,[
                'label' =>'Рейтинг ресторана'
            ])

            ->add('city', TextType::class, array(
                'label' => 'Город*',
                'mapped' => false,
//                'attr' => array(
//                    'placeholder' => 'Например: user@user.ru'
//                ),
                'required' => true,
            ))
            ->add('street', TextType::class, array(
                'label' => 'Улица*',
                'mapped' => false,
//                'attr' => array(
//                    'placeholder' => 'Например: windson5690'
//                ),
                'required' => true
            ))
            ->add('house', TextType::class, array(
                'label' => 'Дом*',
                'mapped' => false,
//                'attr' => array(
//                    'placeholder' => 'Введите фамилию ...',
//                ),
                'required' => true
            ))
            ->add('porch', TextType::class, array(
                'label' => 'Подъезд*',
                'mapped' => false,
//                'attr' => array(
//                    'placeholder' => 'Введите имя ...',
//                ),
                'required' => true
            ))
            ->add('apartment', TextType::class, array(
                    'label' => 'Квартира*',
                    'mapped' => false,
//                'attr' => array(
//                    'placeholder' => 'Например: +79293215614'
//                )
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Restoran::class
        ]);
    }



}