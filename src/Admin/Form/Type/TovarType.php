<?php


namespace Admin\Form\Type;


use App\Entity\Category;
use App\Entity\Restoran;
use App\Entity\Tip;
use App\Entity\Tovar;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormTypeInterface;

class TovarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                'label' =>'Название товара'
            ])
            ->add('cena',MoneyType::class,[
                'label' =>'Цена'
            ])
            ->add('kolichestvo',IntegerType::class,[
                'label' =>'Количество'
            ])
            ->add('coment',TextareaType::class,[
                'label' =>'Название товара'
            ])
            ->add('category', EntityType::class, array(
                    'class' => Category::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'form-control',
                        'style' => 'margin:5px 0;'),

                )
            )
            ->add('tip', EntityType::class, array(
                    'class' => Tip::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'form-control',
                        'style' => 'margin:5px 0;'),

                )
            )
            ->add('restoran', EntityType::class, array(
                    'class' => Restoran::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => true,
                    'mapped' => false,
                    'attr' => array('class' => 'form-control',
                        'style' => 'margin:5px 0;'),

                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tovar::class
        ]);
    }



}