<?php
declare(strict_types=1);

namespace Admin\Form\Type;

use App\Entity\Courier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class CourierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('surname', TextType::class, array(
                'label' => 'Фамилия*',
                'attr' => array(
                    'placeholder' => 'Введите фамилию ...',
                ),
                'required' => true
            ))
            ->add('name', TextType::class, array(
                'label' => 'Имя*',
                'attr' => array(
                    'placeholder' => 'Введите имя ...',
                ),
                'required' => true
            ))
            ->add('midlename', TextType::class, array(
                'label' => 'Отчество*',
                'attr' => array(
                    'placeholder' => 'Введите отчество ...',
                ),
                'required' => true
            ))
            ->add('adres', TextType::class, array(
                'label' => 'Адрес*',
                'attr' => array(
                    'placeholder' => 'Например: г. Красноярск, ул. Ады Лебедевой, 101а',
                ),
                'required' => true
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Электронная почта*',
                'attr' => array(
                    'placeholder' => 'Например: user@user.ru'
                ),
                'required' => true,
            ))
            ->add('username', TextType::class, array(
                'label' => 'Логин*',
                'attr' => array(
                    'placeholder' => 'Например: windson5690'
                ),
                'required' => true
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Пороль*', 'attr' => array(
                    'placeholder' => '********'
                )),
                'second_options' => array(
                    'label' => 'Повторите пороль*',
                    'attr' => array(
                        'placeholder' => '********'
                    ))))
            ->add('phone', TextType::class, array(
                'label' => 'Номер телефона*',
                'attr' => array(
                    'placeholder' => 'Например: +79293215614'
                )))
            ->add('termsAccepted', CheckboxType::class, array(
                'mapped' => false,
                'constraints' => new IsTrue()));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Courier::class
        ]);
    }

}